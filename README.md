## Ttacker

A simple application that helps you manage your tasks. Allows you to create and edit tasks,
track their progress, set deadlines, create notifications for important tasks, create internal tasks! Moreover you can work together with your friends and colleagues assign task on someone.

## Installing:

#### Cloning:
```bash
$ git clone https://bitbucket.org/ksergej/isp4s.git
```
#### Application installing:
```bash
$ cd tracker
$ python3 setup.py install
```
#### Tests running:
```bash
$ python3 setup.py test
```
##Work with users##

You can register new users, login, change current user

```bash
$ ttracker user [command]
```
For example, add new user:
```bash
$ ttracker user add username
```
##Work with lists##

You can create, remove, rename lists

```bash
$ ttracker list [command]
```

Add new list:

```bash
$ ttracker list add New list
```

Show all task lists:

```bash
$ ttracker list all
```
Remove list:

```bash
$ ttracker list remove ListName
```

Add observer:

```bash
$ ttracker list addobser ObserverName
```

##Work with tasks##

```bash
$ ttracker task [command][params]
```

create subtask:

```bash
$ ttracker task addsub [name] [parent_id][params]
```

Add task executor:
```bash
$ ttracker task addexec [task_id][user_name]
```

Remove task executor:
```bash
$ ttracker task remexec [task_id][user_name]
```

##Work with repeating plans##

Activities can repeat. For activity repeating you must create plan:

```bash
$ ttracker plan add [task_id][period][--interval]
```

To show all your plans:
```bash
$ ttracker plan all
```
Edit plan:
```bas
$ ttracker plan edit [id][params]
```
Remove plan:
```bash
$ ttracker plan remove [id]
```
For more information write "-h" or "--help"