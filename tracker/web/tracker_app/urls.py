from django.conf.urls import url, include
from django.contrib.auth import views as auth_views
from . import views

list_patterns = [
    url(r'^new/$', views.add_list, name='list_new'),
    url(r'^edit/(?P<id>\d+)/$', views.edit_list, name='list_edit'),
    url(r'^show/(?P<id>\d+)/$', views.show_list, name='list_show'),
    url(r'^remove/(?P<id>\d+)/$', views.remove_list, name='list_remove'),
]

task_patterns = [
    url(r'^(?P<task_id>\d+)/(?P<brpath>[a-z0-9]+)/status_change/$', views.status_change, name='status_change'),
    url(r'^(?P<task_id>\d+)/remove/$', views.remove_task, name='remove_task'),
    url(r'^(?P<parent_id>\d+)/task/add/$', views.add_task, name='add_task'),
    url(r'^(?P<task_id>\d+)/(?P<path>[a-z0-9]+)/edit/$', views.edit_task, name='edit_task'),
    url(r'^(?P<task_id>\d+)/(?P<path>[a-z0-9]+)/edit_as_executor/$', views.edit_task_as_executor, name='edit_task_as_executor'),
    url(r'^(?P<task_id>\d+)/show/$', views.show_task, name='show_task'),
    url(r'^(?P<task_id>\d+)/(?P<subtask_id>\d+)/removelink/$', views.remove_subtask_link, name='remove_subtask_link'),
    url(r'^(?P<task_id>\d+)/edit_parent', views.edit_parent_task, name='edit_parent'),
]

notification_pattern = [
    url(r'(?P<key>[a-z0-9]+)/show', views.show_notifications, name='show_notifications'),
    url(r'^(?P<list>[a-z0-9]+)/(?P<task_id>\d+)/$', views.add_notification, name='add_notification'),
    url(r'^(?P<list>[a-z0-9]+)/(?P<task_id>\d+)/(?P<notification_id>\d+)/$', views.remove_notification, name='remove_notification'),
    url(r'^(?P<notification_id>\d+)/change_notifications_status', views.change_notifications_status, name='change_notifications_status'),
]


urlpatterns = [
	url(r'^$', views.list_task, name='base'),
    url(r'^signup/$', views.signup, name='signup'),
    url(r'^logout/$', auth_views.logout, {'next_page': 'base'}, name='logout'),
    url(r'^login/$', auth_views.login, name='login'),
    url(r'^accounts/profile/$', views.list_task, name='profile'),
    url(r'^list/(?P<list>[a-z0-9]+)/task$', views.list_task, name='list_task'),
    url(r'^plans/$', views.plans, name='plans'),
    url(r'^list/', include(list_patterns)),
    url(r'^list/(?P<list>[a-z0-9]+)/task/', include(task_patterns)),
    url(r'^subscriber/(?P<list>[a-z0-9]+)/(?P<task_id>\d+)/(?P<subscriber_id>\d+)/$', views.remove_subscriber,
        name='remove_subscriber'),
    url(r'^subscriber/(?P<list>[a-z0-9]+)/(?P<task_id>\d+)/$', views.add_subscriber, name='add_subscriber'),
    url(r'^notifications/', include(notification_pattern)),
    url(r'^label/(?P<list>[a-z0-9]+)/(?P<task_id>\d+)/$', views.add_label, name='add_label'),
    url(r'^label/(?P<list>[a-z0-9]+)/(?P<task_id>\d+)/(?P<label_id>\d+)/$', views.remove_label, name='remove_label'),

    url(r'^plans/add/$', views.add_plan, name='add_plan'),
    url(r'^plans/(?P<plan_id>\d+)/remove$', views.remove_plan, name='remove_plan'),
    url(r'^plans/(?P<plan_id>\d+)/edit$', views.edit_plan,  name='edit_plan'),
    url(r'^comment/(?P<list>[a-z0-9]+)/(?P<task_id>\d+)/add$', views.add_comment, name='add_comment'),
    url(r'^comment/(?P<list>[a-z0-9]+)/(?P<task_id>\d+)/(?P<comment_id>\d+)/remove$', views.remove_comment, name='remove_comment')
]