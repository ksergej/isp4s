from django.apps import AppConfig


class TrackerConfig(AppConfig):
    name = 'tracker_app'
