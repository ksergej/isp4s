from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.http import Http404
from django.conf import settings
from django.forms.widgets import HiddenInput

from lib.exceptions import (
    LibraryError,
    TimeError,
    ObjectNotFoundError,
    AccessError
)
from dateutil.relativedelta import relativedelta
from datetime import timedelta, datetime

import lib.database as database
from lib.library_manager import LibraryManager
from .forms import (
    ListForm,
    TaskForm,
    SubscriberForm,
    NotificatonForm,
    LabelForm,
    PlanForm,
    StatusChangeForm,
    EditParentTaskForm
)

from lib.models import (
    TaskPriority,
    TaskStatus,
    Period
)


def update_tasks_status(func):
    def wrapper(request, *args, **kwargs):
        if request.user.is_authenticated:
            lib_manager = get_lib_manager()
            lib_manager.check_tasks_status(request.user.username)
        return func(request, *args, **kwargs)
    return wrapper


def update_plans(func):
    def wrapper(request, *args, **kwargs):
        if request.user.is_authenticated:
            lib_manager = get_lib_manager()
            lib_manager.update_plans(request.user.username)
        return func(request, *args, **kwargs)
    return wrapper


def error_handler(func):
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except ObjectNotFoundError as e:
            raise Http404(str(e))
        except AccessError as e:
            raise Http404(str(e))
        except LibraryError:
            redirect('tracker_app/list_task')
    return wrapper


def get_lib_manager():
    session = database.get_session("sqlite:///{}".format(settings.DATABASES['default']['NAME']))
    lib_manager = LibraryManager(session)
    return lib_manager


def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=password)
            login(request, user)
            return redirect('list_task', 'nolist')
    else:
        form = UserCreationForm()
    return render(request, 'registration/signup.html', {'form': form})


@update_tasks_status
@update_plans
@error_handler
def list_task(request, list="nolist"):
    lib_manager = get_lib_manager()
    user = request.user.username
    all_lists = lib_manager.get_task_lists(user)
    all_lists += lib_manager.get_task_lists(user, as_observer=True)
    executor_task_ids = []
    can_add_task = "true"
    if list == "archive":
        tasks = lib_manager.get_tasks(user, status=TaskStatus.ARCHIVED)
        can_add_task = "false"
    else:
        if list == "nolist":
            list_id = None
            without_task_list = True
        else:
            list_id = int(list)
            without_task_list = False
            task_list = lib_manager.get_task_list(user, list_id)
            if task_list.user != user:
                can_add_task = "false"

        tasks = lib_manager.get_tasks(
            user,
            task_list_id=list_id,
            as_executor=True,
            without_parent=True,
            without_task_list=without_task_list
        )
        if tasks:
            executor_task_ids = [task.id for task in tasks]
        tasks = lib_manager.get_tasks(
            user, task_list_id=list_id,
            without_parent=True, without_task_list=without_task_list
        )
        tasks += lib_manager.get_tasks(
            user, task_list_id=list_id, as_observer=True,
            without_parent=True, without_task_list=without_task_list
        )
        tasks = sorted(tasks, key=lambda task: task.priority.value, reverse=True)
    return render(request,
                  'tracker_app/list_task.html',
                  {'all_lists': all_lists,
                   'list': list, 'tasks':tasks,
                   'executor_task_ids':executor_task_ids,
                   'can_add_task':can_add_task})


@update_tasks_status
@update_plans
@login_required
@error_handler
def show_list(request, id):
    lib_manager = get_lib_manager()
    list = lib_manager.get_task_list(request.user.username, id)
    return render(request, 'tracker_app/list_show.html', {'list': list})


def add_list(request):
    if request.method == "POST":
        form = ListForm(request.POST)
        if form.is_valid():
            lib_manager = get_lib_manager()
            id = lib_manager.add_task_list(
                request.user.username, form.cleaned_data.get('name')
            )
            return redirect('list_task', list=id)
    else:
        form = ListForm()
    return render(request, 'tracker_app/show_form.html', {'form': form, 'title':'Add list'})

@update_tasks_status
@login_required
@error_handler
def status_change(request, list, task_id, brpath):
    lib_manager = get_lib_manager()
    user = request.user.username
    task = lib_manager.get_task(user, task_id)
    if request.method == 'POST':
        if task.status == TaskStatus.NOT_DONE:
            new_status = TaskStatus.IN_PROGRESS

        elif task.status == TaskStatus.IN_PROGRESS:
            new_status = TaskStatus.EXECUTED

        elif task.status == TaskStatus.FAILED:
            new_status = TaskStatus.EXECUTED

        else:
            new_status = TaskStatus.ARCHIVED

        lib_manager.edit_task_status(user, task_id, new_status)

    if brpath == "taskshow":
        return redirect('show_task', list, task_id)
    elif brpath == "subtask":
        subtask = lib_manager.get_task(user, task_id)
        return redirect('show_task', list, subtask.parent_task_id)
    else:
        return redirect('list_task', list)


@update_tasks_status
@update_plans
@login_required
@error_handler
def show_task(request, list, task_id):
    lib_manager = get_lib_manager()
    user = request.user.username
    lib_manager.get_task_lists(user)
    task = lib_manager.get_task(user, task_id)
    is_executor = False
    if task.parent_task_id is None:
        parent_task = "No parent"
    else:
        parent_task = lib_manager.get_task(user, task.parent_task_id)
    if task.task_list is not None:
        list_str = task.task_list.name
    elif task.status == TaskStatus.ARCHIVED:
        list_str = 'Archive'
    else:
        list_str = 'Without list'

    executor_names = [executor.user for executor in task.executors]
    if user in executor_names:
        is_executor = True

    notifications = lib_manager.get_notifications(user, task_id)
    labels = lib_manager.get_labels(user, task_id=task_id)

    executor_subtask_ids = []
    if task.subtasks:
        subtasks = lib_manager.get_tasks(user, parent_task_id=task_id, as_executor=True)
        executor_subtask_ids = [subtask.id for subtask in subtasks]

    return render(request, 'tracker_app/task_show.html',
                  {'list':list, 'task': task,
                   'is_executor':is_executor,
                   'executor_subtask_ids':executor_subtask_ids,
                   'parent_task':parent_task,
                   'list_str':list_str,
                   'task_notifications':notifications,
                   'task_labels':labels})


def remove_task(request, list, task_id):
    if request.method == 'POST':
        lib_manager = get_lib_manager()
        user = request.user.username
        lib_manager.remove_task(user, task_id)
    return redirect('list_task', list)


@login_required
def add_task(request, list, parent_id):
    lib_manager = get_lib_manager()
    user = request.user.username
    lists = lib_manager.get_task_lists(user)
    choices_list = ([(0, 'without list')] + [(list.id, list.name) for list in lists])
    choices_status_list = [
        (status.value, status.name)
        for status in TaskStatus if status != TaskStatus.TEMPLATE
    ]
    parent_id = int(parent_id)

    if request.method == 'POST':
        form = TaskForm(choices_list, choices_status_list, request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            priority = TaskPriority(int(form.cleaned_data['priority']))
            description = form.cleaned_data['description']
            deadline = form.cleaned_data['deadline']
            list_id = int(form.cleaned_data['list_id'])

            if list_id == 0:
                list_id = None
            if deadline is not None:
                deadline = deadline.replace(tzinfo=None)

            if parent_id == 0:
                lib_manager.add_task(
                    user, name, list_id,
                    description, deadline, priority
                )
            else:
                lib_manager.add_subtask(
                    user, name, parent_id,
                    description, deadline, priority
                )
            if parent_id == 0:
                return redirect('list_task', list)
            else:
                return  redirect('show_task', list, parent_id)

    else:
        if list == 'nolist':
            list_id = 0
        else:
            list_id = int(list)
        form = TaskForm(choices_list, choices_status_list, initial={'list_id':list_id})
        form.fields['status'].widget = HiddenInput()

    return render(request,  'tracker_app/show_form.html', {'form': form, 'title':"Add task"})

@update_tasks_status
@login_required
@error_handler
def edit_task(request, list, task_id, path):
    lib_manager = get_lib_manager()
    user = request.user.username
    task = lib_manager.get_task(user, task_id)
    lists = lib_manager.get_task_lists(user)
    choices_list = ([(0, 'without list')] + [(list.id, list.name) for list in lists])

    if task.status != TaskStatus.TEMPLATE:
        choices_status_list = [
            (status.value, status.name)
            for status in TaskStatus if status != TaskStatus.TEMPLATE
        ]
    else:
        choices_status_list = [(TaskStatus.TEMPLATE.value, TaskStatus.TEMPLATE)]

    if request.method == 'POST':
        form = TaskForm(choices_list, choices_status_list, request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            priority = TaskPriority(int(form.cleaned_data['priority']))
            description = form.cleaned_data['description']
            status = TaskStatus(int(form.cleaned_data['status']))
            deadline = form.cleaned_data['deadline']
            if deadline is not None:
                deadline = deadline.replace(tzinfo=None)

            list_id = int(form.cleaned_data['list_id'])
            if list_id == 0 and task.task_list_id is not None:
                lib_manager.edit_task(
                    user, task_id, name, description,
                    deadline, priority, without_task_list=True
                )
            else:
                lib_manager.edit_task(
                    user, task_id, name, description,
                    deadline, priority
                )
                if list_id != 0 and list_id != task.task_list_id:
                    lib_manager.move_task_to_another_list(user, task_id, list_id)

            if task.status != status:
                lib_manager.edit_task_status(user, task_id, status)
            if path == "listtask":
                return redirect('list_task', list)
            else:
                return  redirect('show_task', list, task_id)
    else:
        list = task.task_list_id
        if list is None:
            list = 0
        form = TaskForm(choices_list, choices_status_list,
            initial={
                'name': task.name,
                'deadline': task.deadline,
                'priority': task.priority.value,
                'description': task.description,
                'status': task.status.value,
                'list_id': list,
            }
        )
    return render(request, 'tracker_app/show_form.html',
                  {'title': "Edit task",
                   'form': form})

@update_tasks_status
@update_plans
@login_required
@error_handler
def edit_parent_task(request, list, task_id):
    lib_manager = get_lib_manager()
    user = request.user.username
    task = lib_manager.get_task(user, task_id)
    parent_tasks = lib_manager.get_tasks(user)
    subtask_ids = lib_manager.get_subtask_ids(task) + [task.id]
    parent_task_choices_list = [(0, 'without parent')]
    parent_task_choices_list += [
        (p_task.id, p_task.name)
        for p_task in parent_tasks
        if (p_task.id not in subtask_ids
            and (p_task.status == TaskStatus.IN_PROGRESS
                or p_task.status == TaskStatus.NOT_DONE)
            )
    ]
    if request.method == 'POST':
        form = EditParentTaskForm(parent_task_choices_list, request.POST)
        if form.is_valid():
            parent_id = int(form.cleaned_data['parent_task_id'])
            if parent_id == 0:
                lib_manager.edit_task(user, task.id, without_parent=True)
            else:
                lib_manager.edit_task(user, task.id, parent_task_id=parent_id)
            return redirect('show_task', list, int(task_id))
    else:
        if task.parent_task_id is None:
            parent_id = 0
        else:
            parent_id = task.parent_task_id
        form = EditParentTaskForm(
            parent_task_choices_list,
            initial={'parent_id':parent_id}
        )
    return render(request, 'tracker_app/show_form.html',
                  {'title': "Edit parent task",
                   'form': form})

@update_tasks_status
@login_required
@error_handler
def edit_task_as_executor(request, list, task_id, path):
    lib_manager = get_lib_manager()
    user = request.user.username
    task = lib_manager.get_task(user, task_id)
    if request.method == 'POST':
        form = StatusChangeForm(request.POST)
        if form.is_valid():
            status = TaskStatus(int(form.cleaned_data['status']))
            lib_manager.edit_task_status(user, task_id, status)
            if path == "listtask":
                return redirect('list_task', list)
            else:
                return redirect('show_task', list, int(task_id))
    else:
        form = StatusChangeForm(
            initial={
                'name': task.name,
                'status': task.status.value,
            }
        )
        form.fields['name'].widget.attrs['readonly'] = True

    return render(request, 'tracker_app/show_form.html',
                  {'title': "Edit task",
                   'form': form })

@update_tasks_status
@login_required
@error_handler
def edit_list(request, id):
    lib_manager = get_lib_manager()
    user = request.user.username
    if request.method == "POST":
        form = ListForm(request.POST)
        if form.is_valid():
            new_name = form.cleaned_data.get('name')
            lib_manager.edit_task_list(user, id, new_name)

            return redirect('list_task', list="nolist")
    else:
        list = lib_manager.get_task_list(user, id)
        form = ListForm(initial={'name': list.name})
    return render(request,
                  'tracker_app/show_form.html',
                  {'form': form})

@login_required
@error_handler
def remove_list(request, id):
    lib_manager = get_lib_manager()
    user = request.user.username
    lib_manager.remove_task_list(user, id)
    return redirect('list_task', list='nolist')


@login_required
@error_handler
def remove_subtask_link(request, list, task_id, subtask_id):
    lib_manager = get_lib_manager()
    user = request.user.username
    lib_manager.edit_task(user, subtask_id, without_parent=True)
    return redirect('show_task', list=list, task_id=task_id)


@login_required
@error_handler
def remove_subscriber(request, list, task_id, subscriber_id):
    lib_manager = get_lib_manager()
    user = request.user.username
    task_id = int(task_id)
    executor = lib_manager.get_subshiber_by_id(subscriber_id)
    if task_id != 0:
        lib_manager.remove_executor(user, executor.user, task_id)
        return redirect('show_task', list=list, task_id=task_id)
    else:
        list_id = int(list)
        lib_manager.remove_observer(user, executor.user, list_id)
        return redirect('list_show', id=list_id)


@login_required
@error_handler
def add_subscriber(request, list, task_id):
    lib_manager = get_lib_manager()
    user = request.user.username
    task_id = int(task_id)

    if request.method == 'POST':
        form = SubscriberForm(request.POST)
        if form.is_valid():
            subscriber_username = form.cleaned_data['username']
            try:
                User.objects.get(username=subscriber_username)
                if task_id != 0:
                    lib_manager.add_executor(user, subscriber_username, task_id)
                else:
                    list = int(list)
                    lib_manager.add_observer(user, subscriber_username, list)
            except User.DoesNotExist:
                form.add_error('username', 'User does not exist')
                return render(request,
                              'tracker_app/show_form.html',
                              {'form': form, 'title': 'Add userlink'})
            except LibraryError as e:
                form.add_error('username', str(e))
                return render(request,
                              'tracker_app/show_form.html',
                              {'form': form, 'title': 'Add userlink'})
            if task_id != 0:
                return redirect('show_task', list=list, task_id=task_id)
            else:
                return redirect('list_show', id=int(list))

    else:
        form = SubscriberForm()
    return render(request, 'tracker_app/show_form.html', {'form': form, 'title': 'Add userlink'})


@update_tasks_status
@update_plans
@login_required
@error_handler
def show_notifications(request, key):
    lib_manager = get_lib_manager()
    user = request.user.username
    lib_manager.get_tasks(user)
    if key == 'current':
        notifications = lib_manager.get_notifications(user,
                                                      date_end= datetime.now(),
                                                      shown=False)
    else:
        notifications = lib_manager.get_notifications(user, shown=True)
    return render(request,
                  'tracker_app/notifications.html',
                  {'notifications': notifications,
                   'key':key})


@login_required
@error_handler
def add_notification(request, list, task_id):
    lib_manager = get_lib_manager()
    user = request.user.username

    if request.method == 'POST':
        form = NotificatonForm(request.POST)
        if form.is_valid():
            text = form.cleaned_data['text']
            date = form.cleaned_data['date'].replace(tzinfo=None)
            print("safa")
            lib_manager.add_notification(user, date, task_id, text)
            return redirect('show_task',
                            list=list,
                            task_id=task_id)
    else:
        form = NotificatonForm()

    return render(request, 'tracker_app/show_form.html',
                  {'form': form, 'title':"add notification"})


@login_required
@error_handler
def remove_notification(request, list, task_id, notification_id):
    lib_manager = get_lib_manager()
    user = request.user.username

    lib_manager.remove_notification(user, notification_id)
    if list == 'notifications':
        if task_id == '0':
            return redirect('show_notifications', 'current')
        else:
            return redirect('show_notifications', 'viewed')

    return redirect('show_task', list=list, task_id=task_id)


@login_required
@error_handler
def change_notifications_status(request, notification_id):
    lib_manager = get_lib_manager()
    user = request.user.username
    lib_manager.change_notification_status(user, notification_id)
    return redirect('show_notifications', 'current')

@login_required
@error_handler
def add_label(request, list, task_id):
    lib_manager = get_lib_manager()
    user = request.user.username

    if request.method == 'POST':
        form = LabelForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']

            lib_manager.add_label(
                user, name, task_id
            )
            return redirect('show_task',
                            list=list,
                            task_id=task_id)
    else:
        form = LabelForm()

    return render(request,
                  'tracker_app/show_form.html',
                  {'form': form, 'title': "Add label"})

@login_required
@error_handler
def remove_label(request, list, task_id, label_id):
    lib_manager = get_lib_manager()
    user = request.user.username
    lib_manager.remove_label(user, label_id, task_id)

    return redirect('show_task', list=list, task_id=task_id)


@login_required
@error_handler
def remove_plan(request, plan_id):
    lib_manager = get_lib_manager()
    user = request.user.username
    lib_manager.remove_plan(user, plan_id)
    return redirect('plans')


@login_required
@error_handler
def add_plan(request):
    lib_manager = get_lib_manager()
    user = request.user.username
    tasks = lib_manager.get_tasks(user, status=TaskStatus.TEMPLATE)
    choices_task_list = [
        (task.id, str(task.id) + " " + task.name + '(Template)')
        for task in tasks
    ]
    tasks = lib_manager.get_tasks(user) + lib_manager.get_tasks(user, as_observer=True)
    choices_task_list += [
        (task.id, str(task.id) + " " + task.name)
        for task in tasks
    ]

    if request.method == 'POST':
        form = PlanForm(choices_task_list, request.POST)
        if form.is_valid():
            period = Period(form.cleaned_data['period'])
            interval = form.cleaned_data['interval']
            task_id = int(form.cleaned_data['task_id'])
            start_time = form.cleaned_data['start_of_creation']
            if start_time is not None:
                start_time = start_time.replace(tzinfo=None)
            execution_time = form.cleaned_data['execution_time']
            print(execution_time)
            try:
                lib_manager.add_plan(
                    user, task_id, period, interval, start_time, execution_time
                )
            except TimeError as e:
                form.add_error('start_of_creation', str(e))
                return render(request,
                       'tracker_app/show_form.html',
                       {'form': form, 'title': "Add plan"})

            return redirect('plans')
    else:
        form = PlanForm(choices_task_list)

    return render(request, 'tracker_app/show_form.html',
           {'form': form, 'title': "Add plan"})


@update_plans
@login_required
@error_handler
def edit_plan(request, plan_id):
    lib_manager = get_lib_manager()
    user = request.user.username
    plan = lib_manager.get_plan(user, plan_id)
    choices_task_list = [(plan.task.id, plan.task.name)]
    if request.method == 'POST':
        form = PlanForm(choices_task_list, request.POST)
        if form.is_valid():
            period = Period(form.cleaned_data['period'])
            interval = form.cleaned_data['interval']
            start_time = form.cleaned_data['start_of_creation']
            if start_time is not None:
                start_time = start_time.replace(tzinfo=None)
                if abs(plan.creation_start - start_time) < timedelta(minutes=1):
                    start_time = None
            execution_time = form.cleaned_data['execution_time']

            if not execution_time:
                execution_time = relativedelta()

            try:
                lib_manager.edit_plan(
                    user, plan_id, period, interval, start_time, execution_time
                )
            except TimeError as e:
                form.add_error('start_of_creation', str(e))
                form.fields['task_id'].widget = HiddenInput()
                return render(request,
                       'tracker_app/show_form.html',
                       {'form': form, 'title': "Edit plan"})

            return redirect('plans')
    else:
        form = PlanForm(
            choices_task_list,
            initial={
                'interval': plan.interval,
                'period': plan.period.value,
                'task_id': plan.task.id,
                'start_of_creation': plan.creation_start,
                'execution_time':plan.execution_time
            }
        )
        form.fields['task_id'].widget = HiddenInput()
    return render(request,'tracker_app/show_form.html',
                  {'form': form,
                   'title': "Edit plan"})


@update_tasks_status
@update_plans
@login_required
@error_handler
def plans(request):
    lib_manager = get_lib_manager()
    user = request.user.username
    lib_manager.update_plans(user)
    plans = lib_manager.get_plans(user)
    return render(request,
                  'tracker_app/plans.html',
                  {'plans': plans})


@login_required
@error_handler
def add_comment(request, list, task_id):
    lib_manager = get_lib_manager()
    user = request.user.username
    print(request.__dict__)
    if request.method == 'POST':
        text = request.POST['comment']
        if text:
            text = text.strip()
            if text != '':
                lib_manager.add_comment(user, text, task_id)
    return redirect('show_task', list=list, task_id=task_id)


@login_required
@error_handler
def remove_comment(request, list, task_id, comment_id):
    lib_manager = get_lib_manager()
    user = request.user.username
    if request.method == 'POST':
        lib_manager.remove_comment(user, comment_id)
    return redirect('show_task', list=list, task_id=task_id)