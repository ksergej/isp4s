from django import forms
from datetime import datetime
from datetime import timedelta
from tempus_dominus.widgets import DateTimePicker
from lib.models import (TaskPriority, TaskStatus, Period)
from dateutil.relativedelta import relativedelta
DATE_FORMAT = '%Y-%m-%d %H:%M'


class ListForm(forms.Form):
    name = forms.CharField(max_length=256)


class TaskForm(forms.Form):
    def __init__(self, choices_list, choices_status_list, *args, **kwargs):
        super(TaskForm, self).__init__(*args, **kwargs)
        self.fields['list_id'] = forms.ChoiceField(
            choices=choices_list,
            label='List'
        )
        self.fields['status'] = forms.ChoiceField(
            choices=choices_status_list,
            required=False
        )

    name = forms.CharField(max_length=200)
    deadline = forms.DateTimeField(
        widget=DateTimePicker(
            options={
                'minDate': (datetime.now() + timedelta(hours=1)).strftime('%Y-%m-%d %H:%M'),
                'useCurrent': False,
            }
        ),
        required=False
    )
    priority = forms.ChoiceField(
        choices=[(task_priority.value, task_priority.name) for task_priority in TaskPriority],
        required=False
    )
    status = forms.ChoiceField()
    description = forms.CharField(
        widget=forms.Textarea,
        required=False
    )
    list_id = forms.ChoiceField()


class SplitDurationWidget(forms.MultiWidget):
    template_name = 'widgets/relativedelta.html'
    def __init__(self, attrs=None):
        if not attrs:
           attrs={}
        attrs.update({'placeholder':''})
        widgets = (forms.NumberInput(attrs=attrs),
                   forms.NumberInput(attrs=attrs),
                   forms.NumberInput(attrs=attrs),
                   forms.NumberInput(attrs=attrs),
                   forms.NumberInput(attrs=attrs))
        super(SplitDurationWidget, self).__init__(widgets, attrs)

    def decompress(self, value):
        if value:
            return [value.years, value.months, value.days, value.hours, value.minutes]
        return [None, None, None, None, None, None]


class MultiValueDurationField(forms.MultiValueField):
    widget = SplitDurationWidget

    def __init__(self, *args, **kwargs):
        fields = (
            forms.IntegerField(min_value=0, required=False),
            forms.IntegerField(min_value=0, required=False),
            forms.IntegerField(min_value=0, required=False),
            forms.IntegerField(min_value=0, required=False),
            forms.IntegerField(min_value=0, required=False)
        )
        super(MultiValueDurationField, self).__init__(
            fields=fields, *args, **kwargs)

    def compress(self, values):
        print(values)
        if not any(values):
            return None
        periods = ['years', 'months', 'days', 'hours', 'minutes']
        time_dict = {}
        for i in range(len(periods)):
            time_dict.update({periods[i]: values[i] if values[i] else 0})
        return relativedelta(**time_dict)


class PlanForm(forms.Form):
    def __init__(self, choices_task_list=None , *args, **kwargs):
        super(PlanForm, self).__init__(*args, **kwargs)
        if choices_task_list is not None:
            self.fields['task_id'] = forms.ChoiceField(
                choices=choices_task_list,
                label='Task'
            )

    period = forms.ChoiceField(
        choices=[(period.value, period.name) for period in Period], required=False
    )
    interval = forms.IntegerField(min_value=1)
    start_of_creation = forms.DateTimeField(
        widget=DateTimePicker(
            options={
                'useCurrent': False,
            }
        ),
        required=False
    )
    execution_time = MultiValueDurationField(required=False)
    task_id = forms.ChoiceField()


class SubscriberForm(forms.Form):
    username = forms.CharField(max_length=250)

class NotificatonForm(forms.Form):
    text = forms.CharField(max_length=250, required=False)
    date = forms.DateTimeField(widget=DateTimePicker(
        options={
            'minDate': (datetime.today() + timedelta(minutes=5)).strftime('%Y-%m-%d %H:%M'),
            'useCurrent': False,
        }
    ))

class LabelForm(forms.Form):
    name = forms.CharField(max_length=50)


class StatusChangeForm(forms.Form):
    name = forms.CharField(max_length=200)
    status = forms.ChoiceField(
        choices=[(status.value, status.name)
                 for status in TaskStatus if status != TaskStatus.TEMPLATE],
        required=False
    )


class EditParentTaskForm(forms.Form):
    def __init__(self, parent_task_choices_list, *args, **kwargs):
        super(EditParentTaskForm, self).__init__(*args, **kwargs)

        self.fields['parent_task_id'] = forms.ChoiceField(
            choices=parent_task_choices_list,
            label='Parent task'
        )
    parent_task_id = forms.ChoiceField()







