from setuptools import (
    setup,
    find_packages
)
setup(
    name="ttracker",
    version="0.1.1",
    install_requires=["sqlalchemy"],
    description="",
    packages=find_packages(),
    entry_points={
        "console_scripts": [
            "ttracker = cli.main:main"
        ]
    },
    test_suite='lib.tests'
)
