"""
    This module contains function for connecting to database
"""

from sqlalchemy import create_engine
import sqlalchemy.orm
from lib.models import Base


def get_session(database_path):
    engine = create_engine(
        database_path,
        connect_args={'check_same_thread': False}
    )
    Session = sqlalchemy.orm.sessionmaker(bind=engine)
    Base.metadata.create_all(engine)
    return Session()