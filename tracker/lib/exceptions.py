"""
    This module contains exception classes used by library
"""

class LibraryError(Exception):
    """Generic error class."""
    pass

class AccessError(LibraryError):
    """Called when the user does not have rights"""
    pass

class ObjectNotFoundError(LibraryError):
    """Called by getting object which doesn"t exist"""
    pass

class TimeError(LibraryError):
    """Exception that reporting about invalid time operation"""
    pass

class AttachError(LibraryError):
    """Exception that reporting about error establishing link between objects"""
    pass

class DetachError(LibraryError):
    """Exception that reporting about error breaking links between objects"""
    pass

class StatusError(LibraryError):
    """Called when task status is not correct"""
    pass