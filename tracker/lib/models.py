"""
   This module contains all models and association table
"""

import enum
from datetime import datetime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import (
    Column,
    Integer,
    String,
    ForeignKey,
    Table,
    DateTime,
    Enum,
    Boolean,
    PickleType
)
from sqlalchemy.orm import (
    relationship,
    backref
)

DATE_TIME_FORMAT = "%d.%m.%Y %H:%M"

Base = declarative_base()

TaskSubscriberAssociation = Table(
    "task_subscriber_association",
    Base.metadata,
    Column("task_id", Integer, ForeignKey("task.id")),
    Column("executor_id", Integer, ForeignKey("subscriber.id"))
)

TaskListObserverAssociation = Table(
    "task_observer_association",
    Base.metadata,
    Column("task_list_id", Integer, ForeignKey("task_list.id")),
    Column("observer_id", Integer, ForeignKey("subscriber.id"))
)

TaskLabelAssociation = Table(
    "task_label",
    Base.metadata,
    Column("task_id", Integer, ForeignKey("task.id")),
    Column("label_id", Integer, ForeignKey("label.id"))
)


class Model(Base):
    """Base model class

       Used as common model for all entities in library.

       Attributes:
           id (int): unique models identifier
           user (str): model creator
       """
    __abstract__ = True

    id = Column(Integer, primary_key=True)
    user = Column(String)

    def __init__(self, user):
        self.user = user


class TaskStatus(enum.Enum):
    """Enum that stores values of task's statuses"""

    NOT_DONE = 0
    IN_PROGRESS = 1
    FAILED = 2
    EXECUTED = 3
    ARCHIVED = 4
    TEMPLATE = 5

    def __str__(self):
        return self.name

    @staticmethod
    def get_from_string(string):
        """Returns TaskStatus object corresponding to string"""
        return TaskStatus[string]


class TaskPriority(enum.Enum):
    """Enum that stores values of task's priorities"""

    LOW = 0
    MEDIUM = 1
    HIGH = 2
    CRITICAL = 3

    def __str__(self):
        return self.name

    @staticmethod
    def get_from_string(string):
        """Returns TaskPriority object corresponding to string"""
        return TaskPriority[string]


class Task(Model):
    """Task model

        Attributes:
            id, user - inherited from Model

            name - task name
            description - detailed description of task
            status - task's execution status
            created_at - time of creation
            deadline - time after which task will fail
            priority - importance of task
            parent_task_id - id for relationship with parent task
            subtasks - subtasks of task
            executors - users who can edit, complete task
            notifications - reminder of uncompleted task
            task_list_id - id for relationship with task_list
            task_list - task_list in which the task is stored
            labels - task themes
            comments - comments
    """
    __tablename__ = "task"

    name = Column(String)
    description = Column(String)
    deadline = Column(DateTime)
    parent_task_id = Column(Integer, ForeignKey("task.id"))
    subtasks = relationship(
        "Task",
        backref=backref('parent', remote_side="Task.id")
    )

    executors = relationship(
        "Subscriber",
        secondary=TaskSubscriberAssociation,
        back_populates="tasks"
    )

    created_at = Column(DateTime, default=datetime.now())
    task_list_id = Column(Integer, ForeignKey("task_list.id"))
    task_list = relationship("TaskList", back_populates="tasks")
    status = Column(Enum(TaskStatus), default=TaskStatus.NOT_DONE)
    priority = Column(Enum(TaskPriority), default=TaskPriority.MEDIUM)
    plans = relationship("Plan", back_populates="task")
    notifications = relationship("Notification", back_populates="task")
    comments = relationship("Comment", back_populates="task")
    labels = relationship(
        "Label",
        secondary=TaskLabelAssociation,
        back_populates="tasks"
    )

    def __init__(self,
                 user,
                 name,
                 description=None,
                 deadline=None,
                 priority=None):
        super(Task, self).__init__(user)

        self.name = name
        self.description = description
        self.deadline = deadline
        self.priority = priority

        if priority is None:
            self.priority = TaskPriority.MEDIUM

    def __str__(self):
        label_names = ', '.join([label.name for label in self.labels])
        comments_ids = (
            ', '.join(
                [str(comment.id) for comment in self.comments]
            )
        )

        executors_str = (
            "\n".join(
                ["Id: {} name {}".format(executor.id, executor.user) for executor in self.executors]
            )
        )
        subtasks_str = (
            "\n".join(
                ["Id: {} name {}".format(subtask.id, subtask.name) for subtask in self.subtasks]
            )
        )
        deadline_str="No deadline"
        if self.deadline is not None:
            deadline_str = self.deadline.strftime(DATE_TIME_FORMAT)

        return ((
            "task id: {} name: {}\ndescription: {}\ndeadline: {}\nstatus: {}\n"
            "priority: {}\nuser: {}\ntask_list_id: {}\nparent_id:{}\nlabels: {}\n"
            "executors:\n{}\nsubtasks:\n{}\ncomments_ids:\n{}\n"
        ).format(
            self.id,
            self.name,
            self.description if self.description else "No description",
            deadline_str,
            self.status,
            self.priority,
            self.user,
            self.task_list_id if self.task_list_id else "No list",
            self.parent_task_id if self.parent_task_id else "No parent",
            label_names if self.labels else "No labels",
            executors_str if self.executors else "No executors",
            subtasks_str if self.subtasks else "No subtasks",
            comments_ids if self.comments else "No comments"
        ))


class TaskList(Model):
    """TaskList model

         Used to group tasks

        Attributes:
            id, user - inherited from Model
            name - list name
            tasks - tasks contained in the list
            observers - users who see all the tasks in the list

    """
    __tablename__ = 'task_list'

    name = Column(String)
    observers = relationship(
        "Subscriber",
        secondary=TaskListObserverAssociation,
        back_populates="task_lists"
    )
    tasks = relationship("Task", back_populates="task_list")

    def __init__(self, user, name):
        super(TaskList, self).__init__(user)
        self.name = name

    def __str__(self):
        observers_str = "\n".join(
            list(map(lambda observer: observer.user, self.observers))
        )
        tasks = [
            task for task in self.tasks if task.parent_task_id is None
        ]
        tasks_str = "\n".join(
            list(map(lambda task: task.name + " id " + str(task.id), tasks))
        )
        return '\n'.join([
            "name: {} id {}",
            "creator: {}",
            "observers: {}",
            "tasks: {}"
        ]).format(
            self.name,
            self.id,
            self.user,
            observers_str if self.observers else "No observers",
            tasks_str if self.tasks else "No tasks"
        )


class Period(enum.Enum):
    """Enum for storing periodicity """

    MINUTES = "minutes"
    HOURS = "hours"
    DAYS = "days"
    WEEKS = "weeks"
    MONTHS = "months"
    YEARS = "years"

    def __str__(self):
        return self.name

    @staticmethod
    def get_from_string(string):
        """Returns Period object corresponding to string"""
        return Period[string]


class Plan(Model):
    """Plan model

         Attributes:
        id, user - inherited from Model
        period - type of interval between tasks creation
        interval - count of period
        last_create - when the task was last created
        execution_time - time for task execution, task.deadline=task.created_at+execution_time
        task_id - id for relationship with task
        task - task as a template

    """

    __tablename__ = "plan"

    period = Column(Enum(Period))
    interval = Column(Integer)
    last_create = Column(DateTime)
    creation_start = Column(DateTime)
    execution_time = Column(PickleType)
    task_id = Column(Integer, ForeignKey("task.id"))
    task = relationship("Task", back_populates="plans")

    def __init__(self, user, task, period, interval, creation_start, execution_time):
        super(Plan, self).__init__(user)
        self.task = task
        self.period = period
        self.interval = interval
        self.creation_start = creation_start
        self.execution_time = execution_time

    def __str__(self):
        return '\n'.join([
            "ID: {}",
            "interval: {}",
            "period: {}",
            "creation start: {}",
            "last create: {}",
            "execution time: {}",
            "task_name: {}",
            "task_id: {}"
        ]).format(
            self.id,
            self.interval,
            self.period,
            self.creation_start,
            self.last_create,
            self.execution_time_str,
            self.task.name,
            self.task_id
        )

    @property
    def execution_time_str(self):
        if self.execution_time:
            period_list = ["years", "months", "days", "hours", "minutes"]
            return " ".join([p[0] + ":"+ str(getattr(self.execution_time, p))
                             for p in period_list if getattr(self.execution_time, p) > 0])
        return "No execution time"


class Notification(Model):
    """Notification model

        Attributes:
            id, user - inherited from Model

            date - time when notification should be shown
            name - notification name
            text - notification text
            is_shown - flag indicates whether notification was viewed

            task_id - id for relationship with task
            task - task
    """

    __tablename__ = "notification"
    text = Column(String)
    date = Column(DateTime)
    is_shown = Column(Boolean, default=False)
    task_id = Column(Integer, ForeignKey('task.id'))
    task = relationship("Task", back_populates="notifications")

    def __init__(self, user, text, date):
        super(Notification, self).__init__(user)
        self.text = text
        self.date = date

    def __str__(self):
        date = self.date.strftime(DATE_TIME_FORMAT)

        return '\n'.join([
            "Notification id: {}",
            "date: {}",
            "task id: {}",
            "is_shown: {}",
            "text:{}"
        ]).format(
            self.id,
            date,
            self.task_id,
            self.is_shown,
            self.text if self.text else "No text"
        )


class Label(Model):
    """Label model

        Attributes:
            id, user - inherited from Model

            name - label name
            tasks - tasks containing this label
    """
    __tablename__ = "label"

    name = Column(String)
    tasks = relationship(
        "Task", secondary=TaskLabelAssociation,
        back_populates="labels"
    )

    def __init__(self, user, name):
        super(Label, self).__init__(user)
        self.name = name

    def __str__(self):
        task_ids = ", ".join([str(task.id) for task in self.tasks])
        return ("Label id={}, name={}, task_ids: {}"
                ).format(self.id, self.name, task_ids)


class Subscriber(Model):
    """Label model

          Attributes:
              id, user - inherited from Model
              user - subscriber name
              task_lists - lists in which. Observer of all tasks in the list
              tasks - tasks that the user has the right to execute and edit
      """
    __tablename__ = "subscriber"

    task_lists = relationship(
        "TaskList",
        secondary=TaskListObserverAssociation,
        back_populates="observers")
    tasks = relationship(
        "Task",
        secondary=TaskSubscriberAssociation,
        back_populates="executors"
    )

    def __init__(self, user):
        super(Subscriber, self).__init__(user)




class Comment(Model):
    """Label model

          Attributes:
              id, user - inherited from Model
              text - message text
              task_id - id for relationship with task
              date - date of creation

    """
    __tablename__ = "comment"

    text = Column(String)
    task_id = Column(Integer, ForeignKey('task.id'))
    task = relationship("Task", back_populates="comments")
    date = Column(DateTime, default=datetime.today())

    def __init__(self, user, text):
        super(Comment, self).__init__(user)
        self.text = text

    def __str__(self):
        date = self.date.strftime(DATE_TIME_FORMAT)
        return '\n'.join([
            "Comment id={}",
            "task_id={}",
            "author={}",
            "date={}",
            "text={}"
        ]).format(
            self.id,
            self.task_id,
            self.user,
            date,
            self.text)
