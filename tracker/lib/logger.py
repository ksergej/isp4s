"""
    Module includes functions for work with logging.
"""

import logging
from functools import wraps

def set_logger(logger_enabled=True, logger_path="./logger.log",
               logger_format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
               logger_level=logging.INFO):
    """
        Sets the logging settings: level, file, format, and turns it on or off.

       :param logger_enabled: the logger is on or off (
       :param logger_level: with what level to log: 'DEBUG'/'INFO'/WARNING'/'ERROR'/'CRITICAL'
       :param logger_path: path to the file in which the logs are written
       :param logger_format: format of the log line

    """

    logger = get_logger()
    if logger_enabled:
        formatter = logging.Formatter(logger_format)
        file_handler = logging.FileHandler(logger_path)
        file_handler.setFormatter(formatter)
        logger.setLevel(logger_level)
        if logger.hasHandlers():
            logger.handlers.clear()
        logger.disabled = False
        logger.addHandler(file_handler)
    else:
        logger.disabled = True


def log_decorator(func):
    """Decorator for class methods, that write method and errors info to log"""

    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            get_logger().info("Function: {}".format(func.__name__))
            get_logger().info("Params: {} {}".format(args, kwargs))
            result = func(*args, **kwargs)
            get_logger().info("Return: {}".format(result))
            return result
        except Exception as e:
            get_logger().exception(e)
            raise e from None
    return wrapper


def get_logger():
    """Returns logger with name 'ttracker'"""
    return logging.getLogger("ttracker")
