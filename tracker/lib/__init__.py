"""
    This package contains database models and manager for interacting with models

    Modules:
        models.py - contains all models
        logger.py - describes decorator that is used to record
                    information about function call
        exceptions.py - exceptions that can occur when working with database
        library_manager.py - logic of working with models
        database.py - creating database models and connecting to database

    Examples:

    Creating Manager instance::

        import datetime

        from lib.library_manager import LibraryManager
        from lib.database import get_sessionn


        lib_manager = LibraryManager(
            session=get_session('sqlite://'),
        )

    Creating new tasks::


        list_id = lib_manager.add_task_list("
            new_user", "new_task_list"
        )

        task_id = lib_manager.add_task(
            "new_user", "new_task", list_id,
            deadline=datetime.datetime(2018, 11, 1, 14, 15)
        )


    Edit task::

        lib_manager.edit_task(
            "new_user", task_id, name="my_task",
             deadline=datetime.datetime(2018, 11, 15, 14, 15)
        )

    Add subtask:

        lib_manager.add_subtask("new_user", "subtask", task_id)

    Edit status:
        from lib.model import TaskStatus

        lib_manager.edit_status("new_user", task_id, TaskStatus.EXECUTED)

    Remove task

        lib_manager.remove_task("new_user", task_id)

"""