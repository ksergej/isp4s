"""
    This module provides functions for all operations and saving to database
"""

from lib.models import (
    Task,
    TaskStatus,
    TaskList,
    Notification,
    Plan,
    Period,
    Comment,
    Subscriber,
    Label
)
import sqlalchemy
import sqlalchemy.orm as orm
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from sqlalchemy import (
    and_,
    or_
)
import lib.logger as logger
from lib.exceptions import (
    LibraryError,
    TimeError,
    ObjectNotFoundError,
    AccessError,
    AttachError,
    DetachError,
    StatusError
)


class LibraryManager:
    """Class LibraryManager contains main functions,
            which need for work with all models.        
         Attributes:
        session: Database session instance.
    """

    def __init__(self, session):
        self.session = session

    @logger.log_decorator
    def add_task(self,
                 user,
                 name,
                 task_list_id=None,
                 description=None,
                 deadline=None,
                 priority=None):
        """Adding task to list

            :param user: task owner
            :param name: task name
            :param task_list_id: id of the list to which the task will be added
            :param description: detailed description of task
            :param deadline: end time of task execution
            :param priority: task priority (TaskPriority class object)

            :return: new task id
        """
        if task_list_id is not None:
            task_list = self.get_task_list(user, task_list_id)
            if task_list.user != user:
                message = "User has no right to edit task_list, id: {}".format(task_list.id)
                raise AccessError(message)

        if deadline:
            if deadline < datetime.now():
                raise TimeError("Deadline is less than today")

        task = Task(user, name, description, deadline, priority)
        task.task_list_id = task_list_id

        self.session.add(task)
        self.session.commit()
        logger.get_logger().info("Added task")
        return task.id

    @logger.log_decorator
    def add_subtask(self,
                    user,
                    name,
                    parent_task_id,
                    description=None,
                    deadline=None,
                    priority=None):
        """Adding subtask to task

                  :param user: task owner
                  :param name: task name
                  :param parent_task_id: id of the task to which the task will be added
                  :param description: detailed description of task
                  :param deadline: end time of task execution
                  :param priority: task priority

                  :return: new task id
              """

        if parent_task_id is None:
            raise LibraryError("parent_task_id is None")

        parent_task = self.get_task(user, parent_task_id)

        if parent_task.user != user:
            raise AccessError("Only the creator of a task can add subtasks")

        self._validate_task_status(parent_task)
        if deadline is not None:
            if deadline < datetime.now():
                raise TimeError("Deadline is less than today")

        task = Task(user, name, description, deadline, priority)

        task.parent_task_id = parent_task_id
        task.task_list = parent_task.task_list

        self.session.add(task)
        self.session.commit()
        logger.get_logger().info("Added subtask")
        return task.id

    def get_task(self, user, task_id):
        """Returns the task by id or throws an exception, 
                if it does not exist, checks the right to read

           :param task_id: id of received task
           :return: task object
        """
        task = self.session.query(Task).filter_by(id=task_id).first()
        if not task:
            raise ObjectNotFoundError("Task not found")

        if (user != task.user
            and (
                not task.task_list
                or all(
                        user != observer.user
                        for observer in task.task_list.observers
                    )
            )
            ):
            raise AccessError("Task not found")
        self.check_tasks_status(user, [task])

        return task

    @logger.log_decorator
    def get_tasks(self,
                  user=None,
                  name=None,
                  status=None,
                  created_by=None,
                  created_to=None,
                  deadline_by=None,
                  deadline_to=None,
                  priority=None,
                  parent_task_id=None,
                  task_list_id=None,
                  as_observer=False,
                  as_executor=False,
                  without_parent=False,
                  without_task_list=False):
        """Returns list of tasks that match specified filters"""
        query = self.session.query(Task).filter(
            and_(
                or_(name is None, Task.name == name),
                or_(priority is None, Task.priority == priority),
                or_(parent_task_id is None, Task.parent_task_id == parent_task_id),
                or_(task_list_id is None, Task.task_list_id == task_list_id),
                or_(not without_parent, Task.parent_task_id == None),
                or_(not without_task_list, Task.task_list == None),
                or_(
                    and_(status is not None, Task.status == status),
                    and_(
                        status is None,
                        Task.status != TaskStatus.ARCHIVED,
                        Task.status != TaskStatus.TEMPLATE
                        )
                    )
            )
        )
        if user is not None:
            if as_observer:
                observer = (self.session.query(Subscriber)
                            .filter_by(user=user)
                            .first())
                if observer:
                    list_ids = [list.id for list in observer.task_lists]
                    query = query.filter(Task.task_list_id.in_(list_ids))
                else:
                    return []
            elif as_executor:
                query = query.join(Task.executors).filter(Subscriber.user==user)
            else:
                query = query.filter(Task.user==user)

        tasks = query.all()
        tasks = [
            task for task in tasks
            if (
                ((created_by is None) or (created_by < task.created_at))
                and((created_to is None) or (created_to > task.created_at))
                and (
                    (deadline_by is None)
                    or (
                        (task.deadline) and (deadline_by < task.deadline)
                    )
                )
                and (
                    (deadline_to is None)
                    or (
                        (task.deadline) and (deadline_to > task.deadline)
                    )
                )
            )
        ]
        return tasks


    def validate_executor_rights(self, user, task):
        """Executors can change the status of task, add notification and labels"""
        if (user != task.user
            and all(
                    user != executor.user
                    for executor in task.executors
                )
        ):
            raise AccessError("No executor right, task id: {}".format(task.id))

    @logger.log_decorator
    def edit_task_status(self, user, task_id, status):
        """Сhanges status of task"""
        task = self.get_task(user, task_id)

        if status is not None:

            self.validate_executor_rights(user, task)

            if task.status == TaskStatus.ARCHIVED:
                raise StatusError("Status archived impossible to change")

            if task.status == TaskStatus.TEMPLATE:
                raise StatusError("Status template impossible to change")

            if (task.status == TaskStatus.EXECUTED
                    and (status == TaskStatus.NOT_DONE
                         or status == TaskStatus.IN_PROGRESS)
                    and task.parent_task_id is not None):
                self.edit_task_status(user, task.parent_task_id, status)

            if status == TaskStatus.EXECUTED:
                self._executed_task(task)

            if status == TaskStatus.ARCHIVED:
                self._archived_task(task)

            task.status = status
            self.session.commit()
            logger.get_logger().info("Task status edited")

    def _archived_task(self, task):

        task.task_list = None
        task.parent_task_id = None
        if task.status != TaskStatus.TEMPLATE:
            task.status = TaskStatus.ARCHIVED

        for notification in task.notifications:
            self.session.delete(notification)

        for subtask in task.subtasks:
            self._archived_task(subtask)

    def _executed_task(self, task):
        task.status = TaskStatus.EXECUTED
        for subtask in task.subtasks:
            self._executed_task(subtask)

    def _validate_task_status(self, task):
        if (task.status != TaskStatus.NOT_DONE
                and task.status != TaskStatus.IN_PROGRESS):
            raise StatusError(
                ("Task has an unsuitable status, name {} id {}"
                 .format(task.name, task.id))
            )

    def check_tasks_status(self, user, tasks=None):
        """Checks the deadline for tasks"""
        if not tasks:
            tasks = self.get_tasks(user=user)

        for task in tasks:
            if (
                task.deadline
                and task.deadline < datetime.now()
                and (
                    task.status == TaskStatus.NOT_DONE
                    or task.status == TaskStatus.IN_PROGRESS
                )
            ):
                task.status = TaskStatus.FAILED
                self.add_notification(
                    task.user,
                    task_id=task.id,
                    text="Task is overdue"
                )
                for executor in task.executors:
                    self.add_notification(
                        executor.user,
                        task_id=task.id,
                        text="Task is overdue"
                    )
        self.session.commit()


    @logger.log_decorator
    def edit_task(self,
                  user,
                  task_id,
                  name=None,
                  description=None,
                  deadline=None,
                  priority=None,
                  parent_task_id=None,
                  without_parent=False):
        """Edits certain fields of task"""

        task = self.get_task(user, task_id)

        if user != task.user:
            raise AccessError("No right to edit task, id: {}".format(task.id))

        if name is not None:
            task.name = name

        if description is not None:
            task.description = description

        if deadline is not None:
            if deadline < datetime.now():
                raise TimeError("Deadline is less than today")
            task.deadline = deadline

        if priority is not None:
            task.priority = priority

        if without_parent:
            task.parent_task_id = None
        else:
            if parent_task_id is not None:
                parent_task = self.get_task(user, parent_task_id)
                if user != parent_task.user:
                    raise AccessError("No right to edit task, id: {}".format(task.id))
                self._validate_task_status(parent_task)

                invalid_task_ids = [task.id]
                invalid_task_ids += self.get_subtask_ids(task)

                if parent_task_id in invalid_task_ids:
                    raise AttachError("It is impossible to change the parent_id, since the loop")

                if parent_task.task_list_id != task.task_list_id:
                    self.move_task_to_another_list(
                        user,
                        task.id, 
                        parent_task.task_list_id
                    )
                task.parent_task_id = parent_task_id

        self.session.commit()
        logger.get_logger().info("Edit task")

    def get_subtask_ids(self, task):
        """Returns task IDs in the tree, 'task' - root"""
        ids = []
        for subtask in task.subtasks:
            ids.append(subtask.id)
            ids += self.get_subtask_ids(subtask)
        return ids

    @logger.log_decorator
    def move_task_to_another_list(self, user, task_id, task_list_id):
        """Moves the task to another list"""
        task = self.get_task(user, task_id)
        if (user != task.user
            or (task.task_list
                and user != task.task_list.user)
        ):
            raise AccessError("No rights to move task")

        if task_list_id:
            task_list = self.get_task_list(user, task_list_id)
            if user != task_list.user:
                raise AccessError("No rights to move task")

        if task.task_list_id != task_list_id:
            self._move_task_to_task_list(task, task_list_id)
            task.parent_task_id = None

        self.session.commit()
        logger.get_logger().info("Task moved")

    def _move_task_to_task_list(self, task, task_list_id, remove_executors=True):
        if remove_executors:
            for executor in task.executors:
                self.remove_executor(task.user, executor.user, task.id)

        for subtask in task.subtasks:
            self._move_task_to_task_list(subtask, task_list_id)

        task.task_list_id = task_list_id
        self.session.commit()

    @logger.log_decorator
    def remove_task(self, user, task_id):
        """Deletes task with subtasks"""
        task = self.get_task(user, task_id)
        if task.user != user:
            raise AccessError("Only the creator can delete the task")

        self._remove_task(user, task)
        logger.get_logger().info("Removed task")

    def _remove_task(self, user, task):
        for plan in task.plans:
            self.session.delete(plan)

        for notification in task.notifications:
            self.session.delete(notification)

        for comment in task.comments:
            self.session.delete(comment)

        for label in task.labels:
            if len(label.tasks) == 1:
                self.session.delete(label)

        for subtask in task.subtasks:
            self._remove_task(user, subtask)

        self.session.delete(task)
        self.session.commit()

    @logger.log_decorator
    def add_task_list(self, user, name):
        """Сreates list from appropriate data set

              :param user: task_list owner
              :param name: task_list name

              :return: new task_list id
        """
        if name is None:
            name = "TaskList"

        task_list = TaskList(user, name)
        self.session.add(task_list)
        self.session.commit()
        logger.get_logger().info("Added list")
        return task_list.id

    @logger.log_decorator
    def edit_task_list(self, user, task_list_id, name):
        """Rename task_list"""
        task_list = self.get_task_list(user, task_list_id)

        if task_list.user != user:
            raise AccessError("User has no right to edit task_list, id: {}".format(task_list.id))

        if name is not None:
            task_list.name = name

        self.session.commit()
        logger.get_logger().info("Edit list")

    @logger.log_decorator
    def remove_task_list(self, user, task_list_id):
        """Deletes list together with task, 
            if user is an observer, then he removes it from the list"""
        task_list = self.get_task_list(user, task_list_id)

        if user == task_list.user:
            without_parent_tasks = [t for t in task_list.tasks if not t.parent_task_id]
            for task in without_parent_tasks:
                self.remove_task(user, task.id)
            self.session.delete(task_list)
            self.session.commit()
            logger.get_logger().info("Removed task_list")
        else:
            observer = (self.session.query(Subscriber)
                        .filter(Subscriber.user == user)
                        .first())
            if observer is not None:
                self.remove_observer(task_list.user, user, task_list.id)

    def get_task_list(self, user, task_list_id):
        """Returns list if the user has access to it"""
        task_list = (self.session.query(TaskList)
                     .filter_by(id=task_list_id)
                     .first())
        if not task_list:
            raise ObjectNotFoundError("TaskList not found")

        if (user != task_list.user
            and all(
                    user != observer.user
                    for observer in task_list.observers
                )
        ):
            raise AccessError("No rights to read task_list")

        return task_list

    def get_task_lists(self, user, as_observer=False):
        """Returns lists where user is creator or observer"""
        if as_observer:
            return (self.session.query(TaskList)
                    .join(TaskList.observers)
                    .filter(Subscriber.user == user).all())
        else:
            return (self.session.query(TaskList)
                    .filter(TaskList.user == user).all())

    @logger.log_decorator
    def add_observer(self, user, observer_user, task_list_id):
        """Adds a new observer to task_list

            :param user: name of the one who adds
            :param observer_user: observer user name
            :param task_list_id: id of the list to which the observer is added

            :return: new task_list id
        """
        if user == observer_user:
            raise AttachError("The name of the observer coincides with the creator")

        task_list = self.get_task_list(user, task_list_id)
        if task_list.user != user:
            raise AccessError("User has no right to edit task_list, id: {}".format(task_list.id))

        observer = (self.session.query(Subscriber)
                     .filter_by(user=observer_user)
                     .first())

        if observer is not None:
            if observer in task_list.observers:
                raise AttachError("Observer has already been added")
        else:
            observer = Subscriber(observer_user)
            self.session.add(observer)

        for obser in task_list.observers:
            self.add_notification(
                obser.user,
                text=("Added observer in list, observer: {}, list: {}"
                        .format(observer.user, task_list.name))
            )

        task_list.observers.append(observer)
        self.add_notification(
            observer.user,
            text=("You were added to the list as an observer, list name {}"
                    .format(task_list.name))
        )

        self.session.commit()
        logger.get_logger().info("Added observer to list")
        return observer.id

    @logger.log_decorator
    def add_executor(self, user, executor_user, task_id):
        """Adds a executor to task

            :param user: name of the one who adds
            :param observer_user: executor user name
            :param task_list_id: id of the task to which the observer is added

            :return: new task_list id
        """

        if user == executor_user:
            raise AttachError("The name of the executor coincides with the creator")

        task = self.get_task(user, task_id)
        if task.task_list is None:
            raise AttachError("Task must be in the tasklist")

        if user != task.task_list.user:
            raise AccessError("Only the list creator can add executors")

        executor = (self.session.query(Subscriber)
                    .filter_by(user=executor_user)
                    .first())

        if executor is not None:
            if executor in task.executors:
                raise AttachError("Executor has already been added")
        else:
            self.add_observer(user, executor_user, task.task_list_id)
            executor = self.get_subscriber(executor_user)

        for exec in task.executors:
            self.add_notification(
                exec.user,
                task_id=task.id,
                text=("Added executor to task, executor:{} task: {}"
                        .format(executor.user, task.name))
            )
        task.executors.append(executor)
        self.add_notification(
            executor.user,
            task_id=task.id,
            text="You were added to the task as an executor"
        )

        self._add_subtasks_executor(task, executor)

        self.session.commit()
        logger.get_logger().info("Added executor to task")
        return executor.id

    def _add_subtasks_executor(self, task, executor):
        """Adds executor to subtasks"""
        for subtask in task.subtasks:
            if executor not in subtask.executors:
                subtask.executors.append(executor)
            self._add_subtasks_executor(subtask, executor)

    def get_subshiber_by_id(self, subscriber_id):
        """Returns subshiber by id"""
        try:
            return (self.session.query(Subscriber)
                    .filter_by(id=subscriber_id)
                    .one())
        except sqlalchemy.orm.exc.NoResultFound:
            raise ObjectNotFoundError("Subscriber not found")

    def get_subscriber(self, subscriber_name):
        """Returns subshiber by name"""
        try:
            return (self.session.query(Subscriber)
                    .filter(Subscriber.user == subscriber_name)
                    .one())
        except sqlalchemy.orm.exc.NoResultFound:
            raise ObjectNotFoundError("Subscriber not found")

    @logger.log_decorator
    def remove_observer(self, user, observer_user, task_list_id):
        """Removes the observer from the list"""
        task_list = self.get_task_list(user, task_list_id)

        if user != task_list.user:
            raise AccessError("Only the list creator can remove observer")

        observer = (self.session.query(Subscriber)
                    .filter_by(user=observer_user)
                    .first())

        if observer is None:
            raise DetachError("User isn't a observer")

        elif observer not in task_list.observers:
            raise DetachError("User isn't a task_list observer")
        else:
            for obser in task_list.observers:
                self.add_notification(
                    obser.user,
                    text=("Observer is removed, observer: {}, list: {}"
                            .format(observer.user, task_list.name))
                )
            observer.task_lists.remove(task_list)
            for task in observer.tasks:
                if task.task_list_id == task_list_id:
                    observer.tasks.remove(task)

            if len(observer.task_lists) == 0 and len(observer.tasks) == 0:
                self.session.delete(observer)

        self.session.commit()
        logger.get_logger().info("Removed observer from list")

    @logger.log_decorator
    def remove_executor(self, user, executor_user, task_id):
        """Removes the executor from the task"""
        task = self.get_task(user, task_id)

        executor = (self.session.query(Subscriber)
                    .filter_by(user=executor_user)
                    .first())

        if user != task.user:
            raise AccessError("Only the task creator can remove executor")

        if executor is None:
            raise DetachError("User isn't a executor")
        elif executor not in task.executors:
            raise DetachError("User isn't a task executor")
        else:
            notifications = self.get_notifications(executor_user, task_id)
            for notification in notifications:
                self.session.delete(notification)
            labels = self.get_labels(executor_user, task_id)
            for label in labels:
                self.remove_label(executor_user, label.id, task_id)
            if (len(executor.task_lists) == 0
                and len(executor.tasks) == 1):
                self.session.delete(executor)
            else:
                task.executors.remove(executor)
            for exec in task.executors:
                self.add_notification(
                    exec.user,
                    task_id=task.id,
                    text=("Executor removed, executor name:{} task: {}"
                            .format(executor.user, task.name))
                )
            self.add_notification(
                executor.user,
                task_id=task.id,
                text=("You were removed from the task as a executor, task: {}"
                        .format( task.name))
            )

        self.session.commit()
        logger.get_logger().info("Removed executor from task")

    def remove_subscriber(self, user, subscriber_name):
        """Removes subscriber"""
        subscriber = (self.session.query(Subscriber)
                    .filter_by(user=subscriber_name)
                    .first())
        if subscriber is not None:
            self.session.delete(subscriber)
            self.session.commit()

    def get_notification(self, user, notification_id):
        """Returns notification by id"""
        try:
            return (self.session.query(Notification)
                    .filter_by(user=user)
                    .filter_by(id=notification_id)
                    .one())
        except sqlalchemy.orm.exc.NoResultFound:
            raise ObjectNotFoundError("Notification not found")

    def get_notifications(self,
                          user,
                          task_id=None,
                          date_begin=None,
                          date_end=None,
                          shown=None):
        """Returns list of notifications that match specified parameters"""
        return (self.session.query(Notification)
                .filter_by(user=user)
                .filter((shown is None) or (Notification.is_shown == shown))
                .filter((task_id is None) or (Notification.task_id == task_id))
                .filter((date_begin is None) or (Notification.date > date_begin))
                .filter((date_end is None) or (Notification.date < date_end))
                .all())

    @logger.log_decorator
    def change_notification_status(self, user, notification_id, shown=True):
        """Sets the notification status to shown"""
        notification = self.get_notification(user, notification_id)
        notification.is_shown = shown

        self.session.commit()
        logger.get_logger().info("Notification status changed")

    @logger.log_decorator
    def add_notification(self,
                         user,
                         date=None,
                         task_id=None,
                         text=None):
        """Сreates notification

            :param user: notification owner
            :param task_id: id of task for which notification should be shown
            :param date: time when notification should be shown
            :param text: notification text

            :return: new notification id
        """
        if not date:
            date = datetime.now()

        notification = Notification(user, text, date)
        if task_id:
            task = self.get_task(user, task_id)
            self.validate_executor_rights(user, task)
            task.notifications.append(notification)

        self.session.add(notification)
        self.session.commit()
        logger.get_logger().info("Added notification")
        return notification.id

    @logger.log_decorator
    def remove_notification(self, user, notification_id):
        """Removes notification by id"""
        notification = self.get_notification(user, notification_id)

        self.session.delete(notification)
        self.session.commit()
        logger.get_logger().info("Removed notification")

    def add_plan(self,
                 user,
                 task_id,
                 period,
                 interval,
                 creation_start=None,
                 execution_time=None):
        """Сreates plan from appropriate data set

             :param user: plan owner
             :param task_id: task id as template example
             :param period: type of interval between tasks creation
                                   (Period class object)
             :param interval: count of periods
             :param creation_start: start of creation
             :param execution_time: time to complete the task,
                                     task_deadline=creation_time+execution_time

             :return: new plan id
        """
        if interval is None or interval <= 0:
            raise TimeError("Invalid interval")

        if (creation_start is not None
                and creation_start < datetime.now()):
            raise TimeError("Invalid creation start")

        if (execution_time is not None
                and abs(execution_time) != execution_time):
            raise TimeError("Invalid execution time")
        elif execution_time == relativedelta():
            execution_time = None

        task = self.get_task(user, task_id)
        if task.status != TaskStatus.TEMPLATE:
            task_template = Task(
                user,
                task.name,
                task.description,
                task.deadline,
                task.priority
            )
            for label in task.labels:
                task_template.labels.append(label)
            task_template.status = TaskStatus.TEMPLATE
            if task.user == user:
                task_template.task_list_id = task.task_list_id
                task_template.parent_task_id = task.parent_task_id
        else:
            task_template = task

        if creation_start is None:
            delta = self.get_plan_delta(period, interval)
            creation_start = datetime.now() + delta
        plan = Plan(
            user,
            task_template,
            period, interval,
            creation_start,
            execution_time
        )

        self.session.add(plan)
        self.session.commit()
        logger.get_logger().info("Added plan")
        return plan.id

    def get_plan(self, user, plan_id):
        """Returns plan by id or raises exception if it doesn't exist"""
        try:
            return (self.session.query(Plan)
                    .filter_by(user=user)
                    .filter_by(id=plan_id).one())

        except sqlalchemy.orm.exc.NoResultFound:
            raise ObjectNotFoundError("Plan not found")

    def get_plans(self, user, task_id=None):
        """Returns list of plan"""
        query = self.session.query(Plan).filter_by(user=user)
        if task_id:
            query = query.filter_by(task_id=task_id)
        return query.all()

    def update_plans(self, user):
        """Create new tasks if it's time"""
        plans = self.get_plans(user)
        for plan in plans:
            if plan.creation_start < datetime.now():
                delta = self.get_plan_delta(plan.period, plan.interval)
                last_create = plan.last_create
                if last_create is None:
                    last_create = plan.creation_start - delta
                while last_create + delta < datetime.now():
                    last_create += delta
                    deadline = None
                    if plan.execution_time:
                        deadline = last_create + plan.execution_time
                    task = Task(
                        plan.task.user,
                        plan.task.name,
                        plan.task.description,
                        deadline,
                        plan.task.priority
                    )
                    task.created_at = last_create
                    task.parent_task_id = plan.task.parent_task_id
                    if task.parent_task_id:
                        parent_task = (self.session.query(Task)
                                       .filter_by(id=task.parent_task_id)
                                       .first())

                        if (parent_task 
                            and parent_task.status == TaskStatus.EXECUTED):
                            self.edit_task_status(
                                user,
                                parent_task.id,
                                TaskStatus.IN_PROGRESS
                            )

                    task.task_list_id = plan.task.task_list_id
                    for label in plan.task.labels:
                        task.labels.append(label)

                    self.session.add(task)
                    self.session.commit()

                    self.add_notification(
                        user,
                        task_id=task.id,
                        text=("New task was created in accordance with plan, plan_id: {}"
                                .format(plan.id))
                    )
                plan.last_create = last_create

        self.session.commit()

    def get_plan_delta(self, period, interval):
        return relativedelta(**{period.value: interval})

    @logger.log_decorator
    def remove_plan(self, user, plan_id):
        """Removes plan by id"""
        plan = self.get_plan(user, plan_id)
        if len(plan.task.plans) == 1:
            self.session.delete(plan.task)

        self.session.delete(plan)
        self.session.commit()
        logger.get_logger().info("Removed plan")

    @logger.log_decorator
    def edit_plan(self,
                  user,
                  plan_id,
                  period=None,
                  interval=None,
                  creation_start=None,
                  execution_time=None):
        """Edits certain fields of plan"""
        plan = self.get_plan(user, plan_id)

        if period is not None:
            plan.period = period

        if interval is not None:
            if interval <= 0:
                raise TimeError("Invalid interval")
            plan.interval = interval

        if creation_start is not None:
            if creation_start < datetime.now():
                raise TimeError("Invalid creation start")
            plan.creation_start = creation_start
            plan.last_create = None

        if execution_time is not None:
            if abs(execution_time) != execution_time:
                raise TimeError("Invalid execution time")
            if execution_time == relativedelta():
                execution_time = None
            plan.execution_time = execution_time

        self.session.commit()
        logger.get_logger().info("Edit plan")

    def get_label(self, user, label_id):
        """Returns label by id or raises exception if it doesn't exist"""
        try:
            return (self.session.query(Label)
                    .filter_by(user=user)
                    .filter_by(id=label_id)
                    .one())

        except sqlalchemy.orm.exc.NoResultFound:
            raise ObjectNotFoundError("Label not found")

    def get_labels(self, user, name=None, task_id=None):
        """Returns list of labels"""
        query = self.session.query(Label).filter_by(user=user)
        if name is not None:
            query = query.filter_by(name=name)
        if task_id is not None:
            query = query.join(Label.tasks).filter(Task.id == task_id)
        return query.all()

    @logger.log_decorator
    def add_label(self, user, name, task_id):
        """Сreates or add label

              :param user: label owner
              :param name: label name
              :param task_id: id of the task to which the label is added

              :return: new label id
        """
        task = self.get_task(user, task_id)
        self.validate_executor_rights(user, task)

        label = (self.session.query(Label)
                .filter_by(user=user)
                .filter_by(name=name)
                .first())

        if label in task.labels:
            raise LibraryError("Label exists")
        elif label is None:
            label = Label(user=user, name=name)
            self.session.add(label)

        label.tasks.append(task)
        self.session.commit()
        logger.get_logger().info("Label added")

        return label.id

    @logger.log_decorator
    def edit_label(self, user, label_id, name):
        """Renamed label"""
        label = self.get_label(user, label_id)
        label.name = name
        self.session.commit()
        logger.get_logger().info("edit label")

    @logger.log_decorator
    def remove_label(self, user, label_id, task_id=None):
        """Removes label"""
        label = self.get_label(user, label_id)
        if task_id is not None:
            task = self.get_task(user, task_id)
            label.tasks.remove(task)
        else:
            self.session.delete(label)
        self.session.commit()
        logger.get_logger().info("Label removed")

    def get_comment(self, comment_id):
        """Returns comment by id"""
        try:
            return (self.session.query(Comment)
                    .filter_by(id=comment_id)
                    .one())
        except sqlalchemy.orm.exc.NoResultFound:
            raise ObjectNotFoundError("Comment not found")

    def get_comments(self, task_id):
        """Returns list comment by task id"""
        return (self.session.query(Comment)
                .filter_by(task_id=task_id)
                .all())

    @logger.log_decorator
    def add_comment(self, user, text, task_id):
        """Сreates new comment

             :param user: comment owner
             :param text: text
             :param task_id: id of the task to which the label is added

             :return: new comment id
        """
        task = self.get_task(user, task_id)

        comment = Comment(user, text)
        comment.task = task

        self.session.add(comment)
        self.session.commit()
        logger.get_logger().info("Comment added")
        return comment.id

    @logger.log_decorator
    def remove_comment(self, user, comment_id):
        """Removes comment"""
        comment = self.get_comment(comment_id)
        if comment.task.user != user and user != comment.user:
            raise AccessError("Comment creator or tasl creator can remove comment")

        self.session.delete(comment)
        self.session.commit()
        logger.get_logger().info("Comment removed")
