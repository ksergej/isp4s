import os
import unittest

import datetime as dt

from lib.models import (
    TaskPriority,
    TaskStatus,
    Period
)
from lib.library_manager import LibraryManager
from lib.logger import set_logger
from lib.exceptions import (
    AccessError,
    TimeError,
    ObjectNotFoundError,
    AttachError,
    StatusError
)
from dateutil.relativedelta import relativedelta
import lib.database as database

DATABASE_NAME = ":test_batabase:"
DATABASE_PATH = "sqlite:///" + DATABASE_NAME

from lib.exceptions import LibraryError
from datetime import timedelta


class TestLibraryManager(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        set_logger(False)

    def setUp(self):
        session = database.get_session(DATABASE_PATH)
        self.lib_manager = LibraryManager(session)

    def tearDown(self):
        os.remove(DATABASE_NAME)

    def test_add_task_list(self):
        task_list_id = self.lib_manager.add_task_list(
            "test_user", "test_task_list"
        )

        task_lists = self.lib_manager.get_task_lists("test_user")
        self.assertEqual(len(task_lists), 1)

        task_list = self.lib_manager.get_task_list("test_user", task_list_id)
        compared_values = (
            (task_list.user, "test_user"),
            (task_list.name, "test_task_list"),
            (len(task_list.observers), 0)
        )
        for value in compared_values:
            self.assertEqual(*value)

    def test_edit_task_list(self):
        list_id = self.lib_manager.add_task_list(
            "test_user", "test_task_list"
        )
        self.lib_manager.edit_task_list("test_user", list_id, "new_name")
        task_list=self.lib_manager.get_task_list("test_user",list_id)
        self.assertEqual(task_list.name, "new_name")

    def test_edit_task_list_as_observer(self):
        list_id = self.lib_manager.add_task_list(
            "test_user",
            "test_task_list"
        )
        self.lib_manager.add_observer(
            "test_user",
            "test_user2",
            list_id
        )
        self.assertRaises(
            AccessError,
            self.lib_manager.edit_task_list,
            "test_user2",
            list_id,
            "new_name"
        )

    def test_get_task_list(self):
        list_id = self.lib_manager.add_task_list(
            "test_user", "test_task_list"
        )
        self.assertRaises(
            ObjectNotFoundError,
            self.lib_manager.get_task_list,
            "test_user",
            list_id+1
        )
        task_list = self.lib_manager.get_task_list(
            "test_user",
            list_id
        )
        self.assertEqual(task_list.name, "test_task_list")

    def test_remove_task_list(self):
        list_id = self.lib_manager.add_task_list(
            "test_user",
            "test_task_list"
        )
        self.lib_manager.remove_task_list(
            "test_user",
            list_id
        )
        lists = self.lib_manager.get_task_lists("test_user")
        self.assertEqual(len(lists), 0)

    def test_remove_task_list_as_observer(self):
        list_id = self.lib_manager.add_task_list(
            "test_user",
            "test_task_list"
        )
        self.lib_manager.add_observer(
            "test_user",
            "test_observer",
            list_id
        )
        lists = self.lib_manager.get_task_lists("test_observer", as_observer=True)
        self.assertEqual(len(lists), 1)
        self.lib_manager.remove_task_list(
            "test_user",
            list_id
        )
        lists = self.lib_manager.get_task_lists("test_observer", as_observer=True)
        self.assertEqual(len(lists), 0)

    def get_task_list_as_observer(self):
        list_id = self.lib_manager.add_task_list(
            "test_user", "test_task_list"
        )
        self.assertRaises(
            AccessError,
            self.lib_manager.get_task_list,
            "test_user2",
            list_id
        )
        self.lib_manager.add_observer(
            "test_user",
            "test_user2",
            list_id
        )

        task_list = self.lib_manager.get_task_list(
            "test_user2",
            list_id
        )
        self.assertEqual(task_list.name, "test_task_list")

    def test_add_task(self):
        task_list_id = self.lib_manager.add_task_list(
            "test_user", "test_task_list")

        self.assertRaises(
            TimeError,
            self.lib_manager.add_task,
            "test_user",
            "test_task",
            task_list_id,
            deadline=dt.datetime(2018, 5, 4)
        )

        task_id = self.lib_manager.add_task(
            "test_user", "test_task", task_list_id
        )

        tasks = self.lib_manager.get_tasks(user="test_user")
        self.assertEqual(len(tasks), 1)

        task = self.lib_manager.get_task("test_user", task_id)
        compared_values = (
            (task.user, "test_user"),
            (task.name, "test_task"),
            (task.task_list_id, task_list_id),
            (task.deadline, None),
            (task.status, TaskStatus.NOT_DONE),
            (len(task.executors), 0),
            (len(task.plans), 0),
            (len(task.comments), 0),
            (task.priority, TaskPriority.MEDIUM),
            (len(task.labels), 0),
            (task.description, None),
            (len(task.notifications), 0)
        )
        for value in compared_values:
            self.assertEqual(*value)

    def test_add_subtask(self):
        task_list_id = self.lib_manager.add_task_list(
            "test_user", "test_task_list")
        task_id_first = self.lib_manager.add_task(
            "test_user", "test_task_first", task_list_id
        )
        self.assertRaises(
            LibraryError,
            self.lib_manager.add_subtask,
            "test_user",
            "test_task_second",
            None
        )
        task_id_second = self.lib_manager.add_subtask(
            "test_user", "test_task_second", task_id_first
        )
        task_first = self.lib_manager.get_task("test_user", task_id_first)
        task_second = self.lib_manager.get_task("test_user", task_id_second)
        self.assertEqual(len(task_first.subtasks), 1)
        self.assertEqual(task_second.parent_task_id, task_id_first)

    def test_edit_task(self):
        task_id = self.lib_manager.add_task(
            "test_user",
            "test_task",
            description="test_description",
            deadline=dt.datetime.today() + dt.timedelta(hours=10),
            priority=TaskPriority.CRITICAL
        )
        self.assertRaises(
            AccessError,
            self.lib_manager.edit_task,
            "invalid_test_user",
            task_id,
            "new_name"
        )
        self.assertRaises(
            TimeError, self.lib_manager.edit_task,
            "test_user", task_id, "new_name",
            deadline=dt.datetime.now() - dt.timedelta(hours=5)
        )

        new_datetime = dt.datetime.now() + dt.timedelta(hours=5)
        self.lib_manager.edit_task(
            "test_user", task_id,
            "new_name",
            deadline=new_datetime,
            priority=TaskPriority.MEDIUM,
            description="test_new_description"
        )
        task = self.lib_manager.get_task("test_user", task_id)
        compared_values = (
            (task.user, "test_user"),
            (task.name, "new_name"),
            (task.deadline, new_datetime),
            (task.priority, TaskPriority.MEDIUM),
            (task.description, "test_new_description"),
        )
        for value in compared_values:
            self.assertEqual(*value)

    def test_edit_task_parent_1(self):
        task_id_1 = self.lib_manager.add_task(
            "test_user",
            "test_task1",
        )
        task_id_2 = self.lib_manager.add_task(
            "test_user",
            "test_task2",
        )
        self.lib_manager.edit_task(
            "test_user",task_id_2, parent_task_id=task_id_1
        )
        task_2 = self.lib_manager.get_task("test_user", task_id_2)
        self.assertEqual(task_2.parent_task_id, task_id_1)

    def test_edit_task_parent_2(self):
        task_id_1 = self.lib_manager.add_task(
            "test_user",
            "test_task1",
        )
        task_id_2 = self.lib_manager.add_task(
            "test_user",
            "test_task2",
        )
        task_id_3 = self.lib_manager.add_task(
            "test_user",
            "test_task3",
        )
        self.lib_manager.edit_task(
            "test_user", task_id_2, parent_task_id=task_id_1
        )
        self.lib_manager.edit_task(
            "test_user", task_id_3, parent_task_id=task_id_2
        )
        self.assertRaises(
            AttachError,
            self.lib_manager.edit_task,
            "test_user",
            task_id_1,
            parent_task_id=task_id_3
        )

    def test_edit_task_parent_3(self):
        task_id_1 = self.lib_manager.add_task(
            "test_user",
            "test_task1",
        )
        task_id_2 = self.lib_manager.add_task(
            "test_user",
            "test_task2",
        )
        self.lib_manager.edit_task_status(
            "test_user",
            task_id_1,
            TaskStatus.EXECUTED
        )
        self.assertRaises(
            LibraryError,
            self.lib_manager.edit_task,
            "test_user",
            task_id_2,
            parent_task_id=task_id_1
        )

    def test_edit_task_status_1(self):
        task_id = self.lib_manager.add_task(
            "test_user",
            "test_task",
        )
        self.lib_manager.edit_task_status(
            "test_user",
            task_id,
            TaskStatus.IN_PROGRESS
        )
        task = self.lib_manager.get_task("test_user", task_id)
        self.assertEqual(task.status, TaskStatus.IN_PROGRESS)

    def test_edit_task_status_2(self):
        task_id_1 = self.lib_manager.add_task(
            "test_user",
            "test_task1",
        )
        task_id_2 = self.lib_manager.add_subtask(
            "test_user",
            "test_task2",
            task_id_1
        )
        self.lib_manager.edit_task_status(
            "test_user",
            task_id_1,
            TaskStatus.EXECUTED
        )
        task_1 = self.lib_manager.get_task("test_user", task_id_1)
        task_2 = self.lib_manager.get_task("test_user", task_id_1)
        self.assertEqual(task_1.status, TaskStatus.EXECUTED)
        self.assertEqual(task_2.status, TaskStatus.EXECUTED)

    def test_edit_task_status_3(self):
        task_id_1 = self.lib_manager.add_task(
            "test_user",
            "test_task1",
        )
        task_id_2 = self.lib_manager.add_subtask(
            "test_user",
            "test_task2",
            task_id_1
        )
        self.lib_manager.edit_task_status(
            "test_user",
            task_id_1,
            TaskStatus.ARCHIVED
        )
        task_1 = self.lib_manager.get_task("test_user", task_id_1)
        task_2 = self.lib_manager.get_task("test_user", task_id_1)
        self.assertEqual(task_1.status, TaskStatus.ARCHIVED)
        self.assertEqual(task_2.status, TaskStatus.ARCHIVED)
        self.assertIsNone(task_2.parent_task_id)

    def test_edit_task_status_4(self):
        task_id_1 = self.lib_manager.add_task(
            "test_user",
            "test_task1",
        )
        task_id_2 = self.lib_manager.add_subtask(
            "test_user",
            "test_task2",
            task_id_1
        )
        self.lib_manager.edit_task_status(
            "test_user",
            task_id_1,
            TaskStatus.EXECUTED
        )
        self.lib_manager.edit_task_status(
            "test_user",
            task_id_2,
            TaskStatus.IN_PROGRESS
        )
        task_1 = self.lib_manager.get_task("test_user", task_id_1)
        task_2 = self.lib_manager.get_task("test_user", task_id_1)
        self.assertEqual(task_1.status, TaskStatus.IN_PROGRESS)
        self.assertEqual(task_2.status, TaskStatus.IN_PROGRESS)

    def test_edit_task_status_5(self):
        task_id_1 = self.lib_manager.add_task(
            "test_user",
            "test_task1",
        )
        self.lib_manager.edit_task_status(
            "test_user",
            task_id_1,
            TaskStatus.ARCHIVED
        )
        self.assertRaises(
            StatusError,
            self.lib_manager.edit_task_status,
            "test_user",
            task_id_1,
            TaskStatus.IN_PROGRESS
        )

    def test_edit_task_parent_4(self):
        list_id = self.lib_manager.add_task_list(
            "test_user",
            "test_list_name"
        )
        task_id_1 = self.lib_manager.add_task(
            "test_user",
            "test_task1",
            list_id
        )
        task_id_2 = self.lib_manager.add_task(
            "test_user",
            "test_task2",
        )
        self.lib_manager.edit_task(
            "test_user", task_id_2, parent_task_id=task_id_1
        )
        task_2 = self.lib_manager.get_task("test_user", task_id_2)
        self.assertEqual(task_2.task_list_id, list_id)

    def test_edit_task_status_as_executor(self):
        list_id = self.lib_manager.add_task_list(
            "test_user",
            "test_list_name"
        )
        task_id = self.lib_manager.add_task(
            "test_user",
            "test_task1",
            list_id
        )
        self.assertRaises(
            AccessError,
            self.lib_manager.edit_task_status,
            "test_executor",
            task_id,
            TaskStatus.IN_PROGRESS
        )

        self.lib_manager.add_executor(
            "test_user",
            "test_executor",
            task_id
        )
        self.lib_manager.edit_task_status(
            "test_executor",
            task_id,
            TaskStatus.IN_PROGRESS
        )
        task = self.lib_manager.get_task("test_user", task_id)
        self.assertEqual(task.status, TaskStatus.IN_PROGRESS)

    def test_move_task_to_another_list(self):
        list_id_1 = self.lib_manager.add_task_list(
            "test_user",
            "test_list_name1"
        )
        list_id_2 = self.lib_manager.add_task_list(
            "test_user",
            "test_list_name2"
        )
        task_id_1 = self.lib_manager.add_task(
            "test_user",
            "test_task1",
            list_id_1
        )
        task_id_2 = self.lib_manager.add_subtask(
            "test_user",
            "test_task2",
            task_id_1
        )
        self.lib_manager.move_task_to_another_list(
            "test_user",
            task_id_1,
            list_id_2
        )
        task_1 = self.lib_manager.get_task("test_user", task_id_1)
        task_2 = self.lib_manager.get_task("test_user", task_id_2)
        self.assertEqual(task_1.task_list_id, list_id_2)
        self.assertEqual(task_2.task_list_id, list_id_2)

        self.lib_manager.move_task_to_another_list(
            "test_user",
            task_id_1,
            None
        )
        task_1 = self.lib_manager.get_task("test_user", task_id_1)
        task_2 = self.lib_manager.get_task("test_user", task_id_2)
        self.assertIsNone(task_1.task_list_id)
        self.assertIsNone(task_2.task_list_id)

        self.lib_manager.move_task_to_another_list(
            "test_user",
            task_id_2,
            list_id_2
        )

        task_1 = self.lib_manager.get_task("test_user", task_id_1)
        task_2 = self.lib_manager.get_task("test_user", task_id_2)
        self.assertIsNone(task_1.task_list_id)
        self.assertIsNotNone(task_2.task_list_id)

    def test_remove_task(self):
        task_id_first = self.lib_manager.add_task(
            "test_user", "test_task_first"
        )
        task_id_second = self.lib_manager.add_subtask(
            "test_user", "test_task_second", task_id_first
        )
        self.lib_manager.remove_task("test_user", task_id_first)
        tasks = self.lib_manager.get_tasks("test_user")
        self.assertEqual(len(tasks), 0)

    def test_remove_task_as_executor(self):
        list_id = self.lib_manager.add_task_list(
            "test_user",
            "test_list_name"
        )
        task_id = self.lib_manager.add_task(
            "test_user",
            "test_task1",
            list_id
        )
        self.lib_manager.add_executor(
            "test_user",
            "test_executor",
            task_id
        )
        self.assertRaises(
            AccessError,
            self.lib_manager.remove_task,
            "test_executor",
            task_id
        )

    def test_add_notification(self):
        task_id = self.lib_manager.add_task(
            "test_user", "test_user"
        )
        notification_id = self.lib_manager.add_notification(
            "test_user",
            dt.datetime.now() + timedelta(minutes=15),
            task_id,
            "Message"
        )

        notifications = self.lib_manager.get_notifications("test_user")
        self.assertEqual(len(notifications), 1)

        notification = self.lib_manager.get_notification(
            "test_user", notification_id
        )
        self.assertEqual(notification.task_id, task_id)
        self.assertIsNotNone(notification.date)
        self.assertEqual(notification.is_shown, False)

    def test_add_notification_as_executor(self):
        list_id = self.lib_manager.add_task_list(
            "test_user",
            "test_list_name"
        )
        task_id = self.lib_manager.add_task(
            "test_user",
            "test_user",
            list_id
        )
        self.lib_manager.add_executor(
            "test_user",
            "test_executor",
            list_id
        )
        notification_id = self.lib_manager.add_notification(
            "test_executor",
            dt.datetime.now() + timedelta(minutes=15),
            task_id,
            "Message"
        )

        notifications = self.lib_manager.get_notifications("test_user")
        self.assertEqual(len(notifications), 0)

        notifications = self.lib_manager.get_notifications("test_executor")
        self.assertNotEqual(len(notifications), 0)

    def test_add_notification_as_observer(self):
        list_id = self.lib_manager.add_task_list(
            "test_user",
            "test_list_name"
        )
        task_id = self.lib_manager.add_task(
            "test_user",
            "test_user",
            list_id
        )
        self.lib_manager.add_observer(
            "test_user",
            "test_observer",
            list_id
        )
        self.assertRaises(
            AccessError,
            self.lib_manager.add_notification,
            "test_observer",
            dt.datetime.now() + timedelta(minutes=15),
            task_id,
            "Message"
        )

    def test_remove_notification(self):
        task_id = self.lib_manager.add_task(
            "test_user", "test_task"
        )
        notification_id = self.lib_manager.add_notification(
            "test_user", dt.datetime.now() + timedelta(hours=15), task_id
        )
        self.lib_manager.remove_notification("test_user", notification_id)
        task = self.lib_manager.get_task("test_user", task_id)
        self.assertEqual(len(task.notifications), 0)

    def test_add_plan(self):
        task_id = self.lib_manager.add_task(
            "test_user", "test_task"
        )
        creation_start = dt.datetime.now() + timedelta(days=10)
        execution_time = relativedelta(weeks=2)
        plan_id = self.lib_manager.add_plan(
            "test_user",
            task_id,
            Period.MINUTES,
            6,
            creation_start,
            execution_time
        )
        plan = self.lib_manager.get_plan("test_user", plan_id)
        self.assertNotEqual(plan.task_id, task_id)
        self.assertEqual(plan.period, Period.MINUTES)
        self.assertEqual(plan.interval, 6)
        self.assertEqual(plan.creation_start, creation_start)
        self.assertEqual(plan.execution_time, execution_time)

    def test_add_plan_invalid_interval(self):
        task_id = self.lib_manager.add_task(
            "test_user", "test_task"
        )
        self.assertRaises(
            TimeError,
            self.lib_manager.add_plan,
            "test_user",
            task_id,
            Period.MINUTES,
            -6,
        )

    def test_add_plan_invalid_creation_start(self):
        task_id = self.lib_manager.add_task(
            "test_user", "test_task"
        )
        self.assertRaises(
            TimeError,
            self.lib_manager.add_plan,
            "test_user",
            task_id,
            Period.MINUTES,
            6,
            dt.datetime.now() - timedelta(hours=5)
        )

    def test_add_plan_invalid_excution_time(self):
        task_id = self.lib_manager.add_task(
            "test_user", "test_task"
        )
        self.assertRaises(
            TimeError,
            self.lib_manager.add_plan,
            "test_user",
            task_id,
            Period.MINUTES,
            -6,
            execution_time=relativedelta()
        )
        self.assertRaises(
            TimeError,
            self.lib_manager.add_plan,
            "test_user",
            task_id,
            Period.MINUTES,
            -6,
            execution_time=relativedelta(month=-1, weeks=5, hours=1)
        )

    def test_add_plan_2(self):
        task_id = self.lib_manager.add_task(
            "test_user", "test_task"
        )
        plan_id = self.lib_manager.add_plan(
            "test_user", task_id, Period.HOURS, 1
        )
        plans = self.lib_manager.get_plans("test_user")
        self.assertEqual(len(plans), 1)
        plan = plans[0]
        self.assertNotEqual(plan.task_id, task_id)
        self.assertEqual(plan.period, Period.HOURS)
        self.assertEqual(plan.interval, 1)
        self.assertIsNotNone(plan.creation_start)
        self.assertIsNone(plan.execution_time)

    def test_edit_plan(self):
        task_id = self.lib_manager.add_task(
            "test_user", "test_task"
        )
        creation_start = dt.datetime.now() + timedelta(days=10)
        execution_time = relativedelta(weeks=2)
        plan_id = self.lib_manager.add_plan(
            "test_user",
            task_id,
            Period.MINUTES,
            6,
            creation_start,
            execution_time
        )
        new_period = Period.HOURS
        new_interval = 19
        new_creation_start = dt.datetime.now() + timedelta(hours=4)
        new_execution_time = relativedelta()
        self.lib_manager.edit_plan(
            "test_user",
            plan_id,
            new_period,
            new_interval,
            new_creation_start,
            new_execution_time
        )
        plan = self.lib_manager.get_plan("test_user", plan_id)
        self.assertEqual(plan.period, new_period)
        self.assertEqual(plan.interval, new_interval)
        self.assertEqual(plan.creation_start, new_creation_start)
        self.assertIsNone(plan.execution_time)

    def test_remove_plan(self):
        task_id = self.lib_manager.add_task(
            "test_user", "test_task"
        )
        plan_id = self.lib_manager.add_plan(
            "test_user", task_id, Period.HOURS, 1
        )
        plans = self.lib_manager.get_plans("test_user")
        self.assertEqual(len(plans), 1)
        self.lib_manager.remove_plan("test_user", plan_id)
        plans = self.lib_manager.get_plans("test_user")
        self.assertEqual(len(plans), 0)

    def test_add_observer(self):
        task_list_id = self.lib_manager.add_task_list(
            "test_user", "test_task_list"
        )
        task_id = self.lib_manager.add_task(
            "test_user", "test_task", task_list_id
        )
        self.lib_manager.add_observer(
            "test_user", "test_user2", task_list_id
        )
        task_list = self.lib_manager.get_task_list(
            "test_user", task_list_id
        )
        self.assertEqual(len(task_list.observers), 1)
        tasks = self.lib_manager.get_tasks("test_user2", as_observer=True)
        self.assertEqual(len(tasks), 1)
        self.assertRaises(
            AccessError,
            self.lib_manager.edit_task_list,
            "test_user2",
            task_id,
            "new_name_task"
        )

    def test_add_executor(self):
        task_list_id = self.lib_manager.add_task_list(
            "test_user", "test_task_list"
        )
        task_id = self.lib_manager.add_task(
            "test_user", "test_task", task_list_id
        )
        self.lib_manager.add_executor(
            "test_user",
            "test_user2",
            task_id
        )
        task_list = self.lib_manager.get_task_list("test_user", task_list_id)
        self.assertEqual(len(task_list.observers), 1)
        tasks = self.lib_manager.get_tasks("test_user2", as_observer=True)
        self.assertEqual(len(tasks), 1)
        tasks = self.lib_manager.get_tasks("test_user2", as_executor=True)
        self.assertEqual(len(tasks), 1)

    def test_add_remove_label(self):
        task_id = self.lib_manager.add_task(
            "test_user", "test_task"
        )
        label_id = self.lib_manager.add_label("test_user", "test_label", task_id)
        labels = self.lib_manager.get_labels("test_user")
        self.assertEqual(len(labels), 1)

        task = self.lib_manager.get_task("test_user", task_id)
        self.assertEqual(len(task.labels), 1)

        label = self.lib_manager.get_label("test_user", label_id)
        compared_values = (
            (label.user, "test_user"),
            (label.name, "test_label"),
            (len(label.tasks), 1)
        )
        for compared_value in compared_values:
            self.assertEqual(*compared_value)

    def test_edit_label(self):
        task_id = self.lib_manager.add_task(
            "test_user", "test_task"
        )
        label_id = self.lib_manager.add_label("test_user", "test_label", task_id)
        self.lib_manager.edit_label("test_user", label_id, "new_name")
        label = self.lib_manager.get_label("test_user", label_id)
        self.assertEqual(label.name, "new_name")

    def test_remove_label(self):
        task_id = self.lib_manager.add_task(
            "test_user", "test_task"
        )
        label_id = self.lib_manager.add_label("test_user", "test_label", task_id)
        self.lib_manager.remove_label("test_user", label_id)
        labels = self.lib_manager.get_labels("test_user")
        self.assertEqual(len(labels), 0)

    def test_comment(self):
        task_id = self.lib_manager.add_task(
            "test_user", "test_task"
        )
        comment_id = self.lib_manager.add_comment("test_user", "my_comment", task_id)
        comments = self.lib_manager.get_comments(task_id)
        self.assertEqual(len(comments), 1)
        self.lib_manager.remove_comment("test_user", comment_id)
        comments = self.lib_manager.get_comments(task_id)
        self.assertEqual(len(comments), 0)

if __name__ == "__main__":
    unittest.main()