import cli.config as config
import lib.logger as logger
import sys
from lib.library_manager import LibraryManager
import cli.parser as parser
from cli.user_controller import UserController
import lib.database as database
from lib.exceptions import LibraryError
from argparse import ArgumentTypeError
from functools import wraps


class AuthorisationError(Exception):
    def __init__(self, message):
        self.message = message


def handle_errors(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except LibraryError as e:
            print("Error in library: {}".format(e), file=sys.stderr)
            sys.exit(1)
        except KeyError as e:
            print("Invalid key: {}".format(e), file=sys.stderr)
            sys.exit(1)
        except ValueError as e:
            print("Invalid value: {}".format(e), file=sys.stderr)
            sys.exit(1)
        except ArgumentTypeError as e:
            print("Invalid console argument: {}".format(e), file=sys.stderr)
            sys.exit(1)
        except AuthorisationError as e:
            print("Authorisation error: {}".format(e), file=sys.stderr)
            sys.exit(1)
        except Exception as e:
            print("Internal error: {}".format(e), file=sys.stderr)
            sys.exit(1)

    return wrapper


@handle_errors
def main():
    logger.set_logger(
        config.LOG_ENABLED,
        config.LOG_PATH,
        config.LOG_FORMAT,
        config.LOG_LEVEL
    )

    session = database.get_session(config.DATABASE_PATH)
    lib_manager = LibraryManager(session)
    user_controller = UserController(session, config.CONFIG_FILE)
    user = user_controller.get_current_user()

    args = parser.parse_args()

    if getattr(args, "object") == "user":
        args.func(args, user_controller)
    elif user is not None:
        if user is not None:
            print("Hello {}!".format(user.name))
        else:
            print("Hello stranger")

        args.func(args, lib_manager, user)
    else:
        raise AuthorisationError("You're not authorized")


if __name__ == "__main__":
    main()
