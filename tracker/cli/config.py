import os
import logging

DATA_DIRECTORY = os.path.dirname(__file__)
DATABASE_PATH = "sqlite:///" + DATA_DIRECTORY + "/database.db"
CONFIG_FILE = os.path.join(DATA_DIRECTORY, "config.ini")
DATE_FORMAT = '%d.%m.%Y_%H:%M'
LOG_PATH = os.path.join(DATA_DIRECTORY, "logger.log")
LOG_ENABLED = True
LOG_FORMAT = "%(asctime)s, %(name)s, [%(levelname)s]: %(message)s"
LOG_LEVEL = logging.INFO
