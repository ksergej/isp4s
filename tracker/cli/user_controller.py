from cli.user import User
import configparser


class UserController:
    def __init__(self, session, config_file):
        self.session = session
        self.config_file = config_file
        self.config_parser = configparser.ConfigParser()

    def login_user(self, username):
        if username is not None:
            user = self.session.query(User).filter_by(name=username).first()
            if user is None:
                user = self.add_user(username)
            self.config_parser['user'] = {}
            self.config_parser['user']['username'] = user.name
            with open(self.config_file, 'w') as configfile:
                self.config_parser.write(configfile)
            return user

    def get_current_user(self):
        try:
            self.config_parser.read(self.config_file)
            username = self.config_parser['user']['username']
            return self.session.query(User).filter_by(name=username).first()
        except Exception:
            pass

    def get_all_users(self):
        return self.session.query(User).all()

    def add_user(self, username):
        user = self.session.query(User).filter_by(name=username).first()
        if user is None:
            user = User(username)
            self.session.add(user)
            self.session.commit()
        else:
            raise ValueError("User %s exists" % username)
        return user

    def logout_user(self):
        self.config_parser['user'] = {}
        with open(self.config_file, 'w') as configfile:
            self.config_parser.write(configfile)