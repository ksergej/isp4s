def get_current_user(args, user_controller):
    current_user = user_controller.get_current_user()
    if current_user is None:
        print("You're not authorized")
    else:
        print(current_user)


def add_user(args, user_controller):
    username = args.username
    user_controller.add_user(username)
    print("Added new user %s" % username)


def logout_user(args, user_controller):
    user_controller.logout_user()


def login_user(args, user_controller):
    username = args.username
    user_controller.login_user(username)


def get_all_users(args, user_controller):
    users = user_controller.get_all_users()
    for user in users:
        print(user)


def get_task_lists(args, lib_manager, user):
    lists = []
    if args.creator is True:
        lists += lib_manager.get_task_lists(user.name)
    if args.observer is True:
        lists += lib_manager.get_task_lists(
            user.name, as_observer=True
        )

    if args.creator is False and args.observer is False:
        lists = (lib_manager.get_task_lists(user.name)
                 + lib_manager.get_task_lists(user.name, as_observer=True))

    if lists:
        for l in lists:
            print("list name: {}, id {}".format(l.name, l.id))
    else:
        print("No lists")


def show_task_list(args, lib_manager, user):
    task_list_id = args.task_list_id
    task_list = lib_manager.get_task_list(
        user.name, task_list_id)
    print(task_list)


def add_task_list(args, lib_manager, user):
    task_list_name = args.name
    task_list_id = lib_manager.add_task_list(
        user.name, task_list_name)
    print("Added new list {0}, id {1}".format(task_list_name, task_list_id))


def remove_task_list(args, lib_manager, user):
    lib_manager.remove_task_list(user.name, args.list_id)
    print("Removed list")


def rename_task_list(args, lib_manager, user):
    task_list_id = args.task_list_id
    new_name = args.newname
    lib_manager.edit_task_list(
        user.name, task_list_id, new_name)
    print("Renamed list")


def show_task(args, lib_manager, user):
    task = lib_manager.get_task(user.name, args.task_id)
    print(task)


def add_task(args, lib_manager, user):
    task_id = lib_manager.add_task(
        user.name,
        args.name,
        args.list_id,
        args.description,
        args.deadline,
        args.prior
    )
    print("Added task, task id {}".format(task_id))


def add_subtask(args, lib_manager, user):
    task_id = lib_manager.add_subtask(
        user.name,
        args.name,
        args.parent_id,
        args.description,
        args.deadline,
        args.prior
    )
    print("Added subtask, task id {}".format(task_id))


def get_tasks(args, lib_manager, user):
    lib_manager.update_plans(user.name)
    tasks = lib_manager.get_tasks(
        user.name,
        args.name,
        args.status,
        args.begin,
        args.end,
        args.created_begin,
        args.created_end,
        args.prior,
        args.parent_id,
        args.list_id,
        args.observer,
        args.executor,
        args.without_parent,
        args.without_list
    )
    if len(tasks) != 0:
        for task in tasks:
            print(task)
    else:
        print("No tasks")


def _print_tree(tab_size, task):
    print("\t" * tab_size, "id: {}, name: {}".format(task.id, task.name))
    for sub_task in task.subtasks:
        _print_tree(tab_size + 1, sub_task)


def print_task_tree(args, lib_manager, user):
    task = lib_manager.get_task(user.name, args.id)
    _print_tree(0, task)


def edit_task(args, lib_manager, user):
    lib_manager.edit_task(
        user.name,
        args.id,
        args.name,
        args.description,
        args.deadline,
        args.prior,
        args.parent_id,
        args.without_parent
    )


def edit_task_status(args, lib_manager, user):
    lib_manager.edit_task_status(
        user.name, args.id, args.status
    )


def move_task(args, lib_manager, user):
    lib_manager.move_task_to_another_list(user.name, args.id, args.list_id)
    print("Task moved")


def remove_task(args, lib_manager, user):
    lib_manager.remove_task(user.name, args.id)
    print("Removed task")


def add_task_executor(args, lib_manager, user):
    id = lib_manager.add_executor(
        user.name, args.name, args.id)
    print("Add executor, id {}".format(id))


def remove_task_executor(args, lib_manager, user):
    lib_manager.remove_executor(
        user.name, args.name, args.id)
    print("Removed executor")


def add_task_list_observer(args, lib_manager, user):
    id = lib_manager.add_observer(
        user.name, args.name, args.id)
    print("Add observer, id {}".format(id))


def remove_task_list_observer(args, lib_manager, user):
    lib_manager.remove_observer(args.name, args.id)
    print("Removed observer")


def add_plan(args, lib_manager, user):
    id = lib_manager.add_plan(
        user.name,
        args.task_id,
        args.period,
        args.interval
    )
    print("Added new plan, id {}".format(id))


def edit_plan(args, lib_manager, user):
    lib_manager.edit_plan(
        user.name,
        args.id,
        args.period,
        args.interval
    )
    print("Edit plan")


def remove_plan(args, lib_manager, user):
    lib_manager.remove_plan(user.name, args.id)
    print("Removed plan")


def get_all_plans(args, lib_manager, user):
    plans = lib_manager.get_plans(user.name)
    if len(plans) != 0:
        for plan in plans:
            print(plan)
    else:
        print("No plans")


def update_plans(args, lib_manager, user):
    lib_manager.update_plans(user.name)


def add_label(args, lib_manager, user):
    label_id = lib_manager.add_label(
        user.name,
        args.name,
        args.task_id
    )
    print("Label with id {} added.".format(label_id))


def get_label(args, lib_manager, user):
    label = lib_manager.get_label(user.name, args.label_id)
    print(label)


def get_labels(args, lib_manager, user):
    labels = lib_manager.get_labels(
        user.name,
        args.name,
        args.task_id
    )
    if labels:
        for label in labels:
            print(label)
            print()
    else:
        print("No labels")


def edit_label(args, lib_manager, user):
    lib_manager.edit_label(user.name, args.label_id, args.name)
    print("Edit label")


def remove_label(args, lib_manager, user):
    lib_manager.remove_label(user.name, args.label_id, args.task_id)
    print("Task label is removed")


def add_notification(args, lib_manager, user):
    notification_id = lib_manager.add_notification(
        user.name,
        args.date,
        args.task_id,
        args.text
    )
    print("Notification added, id {}".format(notification_id))


def remove_notification(args, lib_manager, user):
    lib_manager.remove_notification(
        user.name, args.notification_id)
    print("Notification removed")


def show_notification(args, lib_manager, user):
    notification = lib_manager.get_notification(
        user.name, args.notification_id
    )
    print(notification)


def find_notifications(args, lib_manager, user):
    lib_manager.check_tasks_status(user.name)
    lib_manager.update_plans(user.name)
    notifications = lib_manager.get_notifications(
        user.name,
        args.task_id,
        args.date_begin,
        args.date_end,
        args.shown
    )
    if notifications:
        for notification in notifications:
            print(notification)
            print()
    else:
        print("No notifications")


def change_notification_status(args, lib_manager, user):
    lib_manager.change_notification_status(
        user.name, args.notification_id
    )


def add_comment(args, lib_manager, user):
    comment_id = lib_manager.add_comment(
        user.name, args.text, args.task_id)
    print("Comment added, id {}".format(comment_id))


def remove_comment(args, lib_manager, user):
    lib_manager.remove_comment(args.comment_id)


def show_task_comment(args, lib_manager, user):
    comments = lib_manager.get_comments(args.task_id)
    for comment in comments:
        print(comment)
