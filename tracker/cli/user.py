from lib.models import Base
from sqlalchemy import (
    Column,
    Integer,
    String
)


class User(Base):

    __tablename__ = "user"
    id = Column(Integer, primary_key=True)
    name = Column(String)

    def __init__(self, username):
        self.name = username

    def __str__(self):
        return " ".join(["Id:", str(self.id),"name:", self.name])
