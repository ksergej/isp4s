import argparse
import sys

import cli.handlers as handlers
from datetime import datetime
from lib.models import (
    TaskStatus,
    TaskPriority,
    Period
)

DATE_FORMAT = '%d.%m.%Y_%H:%M'
INPUT_DATE_EXAMPLE = datetime.now().strftime(DATE_FORMAT)


def convert_str_to_datetime(text):
    try:
        return datetime.strptime(text, DATE_FORMAT)
    except ValueError:
        raise argparse.ArgumentTypeError("Invalid date format")


class DefaultHelpParser(argparse.ArgumentParser):
    def error(self, message):
        print('error: %s\n' % message, file=sys.stderr)
        self.print_help()
        sys.exit(2)


def parse_args():

    parser = DefaultHelpParser(
        prog="ttracker",
        description="Simple console task manager"
    )
    subparser = parser.add_subparsers(
        title="Commands",
        help="Choose object you want to work with",
        dest="object"
    )
    user = subparser.add_parser(
        "user",
        help='Manage users'
    )
    user_parser = user.add_subparsers()
    init_user_parser(user_parser)

    task_lists = subparser.add_parser(
        "list",
        help="Manage user lists"
    )
    task_lists_parser = task_lists.add_subparsers()

    def task_lists_help(*args):
        return task_lists.print_help()

    task_lists.set_defaults(func=task_lists_help)
    init_task_list_parser(task_lists_parser)

    task = subparser.add_parser("task", help="Manage tasks")
    task_parser = task.add_subparsers()

    def task_help(*args):
        return task.print_help()

    task.set_defaults(func=task_help)
    init_task_parser(task_parser)

    plan = subparser.add_parser("plan", help="Manage task plans")
    plan_parser = plan.add_subparsers()

    def print_plan_help(*args):
        return plan.print_help()

    plan.set_defaults(func=print_plan_help)
    init_plan_parser(plan_parser)

    label = subparser.add_parser(
        "label", help="Manage labels")
    label_subparser = label.add_subparsers()

    def print_label_help(*args):
        return label.print_help()

    label.set_defaults(func=print_label_help)
    init_label_subparser(label_subparser)

    notification = subparser.add_parser(
        "notification", help="Manage notifications")
    notification_subparser = notification.add_subparsers()

    init_notification_subparser(notification_subparser)

    comment = subparser.add_parser(
        "comment",
        help="Manage comments"
    )
    comment_subparser = comment.add_subparsers()

    def print_comment_help(*args):
        return comment.print_help()

    comment.set_defaults(func=print_comment_help)
    init_comment_subparser(comment_subparser)

    def print_main_help(*args):
        return parser.print_help()

    parser.set_defaults(func=print_main_help)

    args = parser.parse_args()
    return args


def init_user_parser(parser):

    add = parser.add_parser("add", help="Add new user")
    add.add_argument("username", help="New user name")
    add.set_defaults(func=handlers.add_user)

    login = parser.add_parser('login', help='Authorizes user by username')
    login.add_argument('username')
    login.set_defaults(func=handlers.login_user)

    logout = parser.add_parser("logout", help="Logouts user")
    logout.set_defaults(func=handlers.logout_user)

    current = parser.add_parser("current", help="Shows current user")
    current.set_defaults(func=handlers.get_current_user)

    all = parser.add_parser("all", help="Shows all users")
    all.set_defaults(func=handlers.get_all_users)


def init_task_list_parser(parser):

    all = parser.add_parser("all", help="Get action and task lists")
    all.add_argument("-c", "--creator",
                     help="Where am I the creator", action="store_true")
    all.add_argument("-o", "--observer",
                     help="Where I am an observer", action="store_true")
    all.set_defaults(func=handlers.get_task_lists)

    show = parser.add_parser("show", help="Show task list content")
    show.add_argument("task_list_id", type=int, help="TaskList id")
    show.set_defaults(func=handlers.show_task_list)

    add = parser.add_parser("add", help="Add new task list")
    add.add_argument("name", help="TaskList name")
    add.set_defaults(func=handlers.add_task_list)

    remove = parser.add_parser("remove", help="Delete task list")
    remove.add_argument("list_id", type=int, help="List id")
    remove.set_defaults(func=handlers.remove_task_list)

    rename = parser.add_parser("rename", help="Rename task list")
    rename.add_argument("task_list_id", type=int, help="TaskList id")
    rename.add_argument("newname", help="New name")
    rename.set_defaults(func=handlers.rename_task_list)

    add_observer = parser.add_parser(
        "addobser", help="Add task list observer")
    add_observer.add_argument("id", type=int, help="TaskList ID")
    add_observer.add_argument("name", help="Observer name")
    add_observer.set_defaults(func=handlers.add_task_list_observer)

    remove_observer = parser.add_parser(
        "remobser", help="Remove task list observer")
    remove_observer.add_argument("id", type=int, help="TaskList ID")
    remove_observer.add_argument("name", help="Observer name")
    remove_observer.set_defaults(func=handlers.remove_task_list_observer)


def init_task_parser(parser):
    add = parser.add_parser("add", help="Create new task")
    add.add_argument("name", help="Name")
    add.add_argument("-l", "--list_id", type=int, help="TaskList id")

    add.add_argument("-d", "--deadline", type=convert_str_to_datetime,
                     help=("Tasks with specific deadline. Input: '{}'"
                           .format(datetime.now().strftime(DATE_FORMAT)))
    )
    add.add_argument("-de", "--description", help="Task description")
    add.add_argument("-p", "--prior", type=TaskPriority.get_from_string,
                     choices=list(TaskPriority), help="Task priority")
    add.set_defaults(func=handlers.add_task)


    addsub = parser.add_parser("addsub", help="Create subtask")
    addsub.add_argument("name", help="Name")
    addsub.add_argument("parent_id", type=int, help="Parent task id")

    addsub.add_argument(
        "-d", "--deadline",
        type=convert_str_to_datetime,
        help="Tasks with specific deadline. Input example: '{}'".format(INPUT_DATE_EXAMPLE)
    )
    addsub.add_argument("-de", "--description", help="Task description")
    addsub.add_argument(
        "-p", "--prior",
        type=TaskPriority.get_from_string,
        choices=list(TaskPriority), help="Task priority"
    )
    addsub.set_defaults(func=handlers.add_subtask)

    find = parser.add_parser("find", help="Find tasks by specific params")
    find.add_argument("-n", "--name", help="Name")
    find.add_argument(
        "-b", "--begin",
        type=convert_str_to_datetime,
        help="Find tasks with deadline later. Input example: '{}'".format(INPUT_DATE_EXAMPLE)
    )
    find.add_argument(
        "-e", "--end",
        type=convert_str_to_datetime,
        help="Find tasks with deadline earlier. Input example: '{}'".format(INPUT_DATE_EXAMPLE)
    )
    find.add_argument(
        "-cb", "--created_begin",
        type=convert_str_to_datetime,
        help="Find tasks with created_at later. Input example: '{}'".format(INPUT_DATE_EXAMPLE)
    )
    find.add_argument(
        "-ce", "--created_end",
        type=convert_str_to_datetime,
        help="Find tasks with created_at earlier. Input example: '{}'".format(INPUT_DATE_EXAMPLE)
    )
    find.add_argument(
        "-s", "--status",
        type=TaskStatus.get_from_string,
        choices=list(TaskStatus), help="Find tasks by status"
    )
    find.add_argument(
        "-p", "--prior",
        type=TaskPriority.get_from_string,
        choices=list(TaskPriority), help="Find task by priority"
    )
    find.add_argument("-pa", "--parent_id", type=int, help="Parent task ID")
    find.add_argument("-l", "--list_id", type=int, help="task_list id")
    find.add_argument(
        "-ex", "--executor",
        help="Where am I the executor", action="store_true"
    )
    find.add_argument(
        "-o", "--observer",
        help="Where I am an observer", action="store_true"
    )
    find.add_argument(
        "-wp","--without_parent",
        help="Without parent", action="store_true"
    )
    find.add_argument(
        "-wl", "--without_list",
        help="Without tasklist", action="store_true"
    )

    find.set_defaults(func=handlers.get_tasks)

    show = parser.add_parser("show", help="Show task")
    show.add_argument("task_id", type=int, help="Task id")
    show.set_defaults(func=handlers.show_task)

    tree = parser.add_parser("tree", help="Print subtasks tree")
    tree.add_argument("id", help="Task ID")
    tree.set_defaults(func=handlers.print_task_tree)

    edit = parser.add_parser("edit", help="Edit task")
    edit.add_argument("id", help="Task ID")
    edit.add_argument("-n", "--name", help="Name")
    edit.add_argument(
        "-d", "--deadline",
        type=convert_str_to_datetime,
        help="Deadline. Input example: '{}'".format(INPUT_DATE_EXAMPLE)
    )
    edit.add_argument(
        "-de", "--description",
        help="New description"
    )
    edit.add_argument(
        "-p", "--prior",
        type=TaskPriority.get_from_string,
        choices=list(TaskPriority), help="Task priority"
    )
    edit.add_argument("-pa", "--parent_id", type=int, help="Parent task ID")
    edit.add_argument("-wp","--without_parent",
        help="Without parent", action="store_true")
    edit.set_defaults(func=handlers.edit_task)

    edit_status = parser.add_parser("edstatus", help="Edit task status")
    edit_status.add_argument("id", help="Task ID")
    edit_status.add_argument(
        "status",
        type=TaskStatus.get_from_string,
        choices=list(TaskStatus), help="Task status"
    )
    edit_status.set_defaults(func=handlers.edit_task_status)

    move = parser.add_parser("move", help="Moves task to another task_list")
    move.add_argument("id", type=int, help="Task ID")
    move.add_argument("-l","--list_id", type=int, help="task_list id")
    move.set_defaults(func=handlers.move_task)

    remove = parser.add_parser("remove", help="Remove task")
    remove.add_argument("id", type=int, help="Task ID")
    remove.set_defaults(func=handlers.remove_task)

    add_executor = parser.add_parser("addexec", help="Add task executor")
    add_executor.add_argument("id", type=int, help="Task ID")
    add_executor.add_argument("name", help="Executor name")
    add_executor.set_defaults(func=handlers.add_task_executor)

    remove_executor = parser.add_parser("remexec", help="Remove task executor")
    remove_executor.add_argument("id", type=int, help="Task ID")
    remove_executor.add_argument("name", help="Executor name")
    remove_executor.set_defaults(func=handlers.remove_task_executor)

    add_comment = parser.add_parser("addcom", help="Add comment")
    add_comment.add_argument("task_id", type=int, help="Task id")
    add_comment.add_argument("text", help="Message")
    add_comment.set_defaults(func=handlers.add_comment)


def init_plan_parser(parser):
    add = parser.add_parser("add", help="Add new plan")
    add.add_argument("task_id", help="Task ID")
    add.add_argument(
        "period",
        type=Period.get_from_string,
        choices=list(Period),
        help="Task period"
    )
    add.add_argument(
        "-i", "--interval", type=int,
        help="Periods count before next repeating", default=1
    )
    add.set_defaults(func=handlers.add_plan)

    edit_plan = parser.add_parser("edit", help="Edit plan")
    edit_plan.add_argument("id", type=int, help="Plan ID")
    edit_plan.add_argument(
        "-p", "--period", type=Period.get_from_string,
        choices=list(Period), help="New period"
    )
    edit_plan.add_argument(
        "-i", "--interval", type=int,
        help="Periods count before next repeating", default=1
    )
    edit_plan.set_defaults(func=handlers.edit_plan)

    remove = parser.add_parser("remove", help="Remove plan")
    remove.add_argument("id", type=int, help="PLan ID")
    remove.set_defaults(func=handlers.remove_plan)

    all = parser.add_parser("all", help="Show all user plans")
    all.set_defaults(func=handlers.get_all_plans)

    update = parser.add_parser("update", help="Update all user plans")
    update.set_defaults(func=handlers.update_plans)


def init_label_subparser(label_subparser):

    label_show = label_subparser.add_parser("show", help="Shows label details")
    label_show.add_argument("label_id", type=int, help="Label id")
    label_show.set_defaults(func=handlers.get_label)

    labels = label_subparser.add_parser("all", help="Shows labels")
    labels.add_argument(
        "-t", "--task_id", type=int,
        help="Labels for specific task"
    )
    labels.add_argument("-n", "--name", help="labels with specific name")
    labels.set_defaults(func=handlers.get_labels)

    label_add = label_subparser.add_parser("add", help="Add new label")
    label_add.add_argument("name", type=str, help="Label name")
    label_add.add_argument(
        "task_id", type=int,
         help="Id of task for label add"
    )
    label_add.set_defaults(func=handlers.add_label)

    label_edit = label_subparser.add_parser("edit", help="Edits label details")
    label_edit.add_argument("label_id", type=int, help="Label id")
    label_edit.add_argument("name", type=str, help="New label name")

    label_edit.set_defaults(func=handlers.edit_label)

    label_remove = label_subparser.add_parser(
        "remove", help="Removes label, when task_id is specified, deleting a label from the task"
    )
    label_remove.add_argument("label_id", type=int, help="Label id")
    label_remove.add_argument(
        "-t", "--task_id", type=int,
        help="Task id, from which to remove"
    )
    label_remove.set_defaults(func=handlers.remove_label)


def init_notification_subparser(notification_subparser):

    show = notification_subparser.add_parser(
        "show", help="Shows notification"
    )
    show.add_argument("notification_id", type=int, help="Notification id")
    show.set_defaults(func=handlers.show_notification)

    find = notification_subparser.add_parser(
        "find", help="Shows notifications with date"
    )
    find.add_argument(
        "-db", "--date_begin",
        type=convert_str_to_datetime,
        help="Notifications date later date_begin. Input: '{}'".format(INPUT_DATE_EXAMPLE)
    )
    find.add_argument(
        "-de", "--date_end",
        type=convert_str_to_datetime,
        help="Notifications date earlier date_end. Input: '{}'".format(INPUT_DATE_EXAMPLE)
    )
    find.add_argument(
        "-s", "--shown",
        action="store_true",
        help="Notifications that have been shown"
    )
    find.add_argument(
        "-t", "--task_id",
        type=int, help="Notifications for task"
    )
    find.set_defaults(func=handlers.find_notifications)

    add = notification_subparser.add_parser(
        "add", help="Added new task notification"
    )
    add.add_argument("task_id", type=int, help="Task id")
    add.add_argument(
        "date", type=convert_str_to_datetime,
        help=("Time when notification will be shown. Input example: '{}'"
            .format(INPUT_DATE_EXAMPLE))
    )
    add.add_argument("-t", "--text", type=str, help="Notification text")
    add.set_defaults(func=handlers.add_notification)

    remove = notification_subparser.add_parser(
        "remove", help="Remove notification"
    )
    remove.add_argument("notification_id", type=int, help="Notification id")
    remove.set_defaults(func=handlers.remove_notification)

    view = notification_subparser.add_parser(
        "view", help="set remind status as shown"
    )
    view.add_argument("notification_id", type=int, help="Notification id")
    view.set_defaults(func=handlers.change_notification_status)


def init_comment_subparser(parser):

    add = parser.add_parser("add", help="Added comment to task")
    add.add_argument("task_id", type=int, help="Task ID")
    add.add_argument("text", help="Message")
    add.set_defaults(func=handlers.add_comment)

    remove = parser.add_parser("remove", help="Removed comment from task")
    remove.add_argument("comment_id", type=int, help="Comment id")
    remove.set_defaults(func=handlers.remove_comment)

    show = parser.add_parser("show", help="show comment in task")
    show.add_argument("task_id", type=int, help="Task id")
    show.set_defaults(func=handlers.show_task_comment)
